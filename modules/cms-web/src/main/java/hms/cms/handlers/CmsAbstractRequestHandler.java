/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.handlers;

import hms.cms.handlers.download.CmsDownloadUrlHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.common.Utils.getExpireDate;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsInternalRepositoryService;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;
import static hms.cms.service.CmsServiceRegistry.*;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class CmsAbstractRequestHandler implements CmsRequestHandler {

    Logger logger = LoggerFactory.getLogger(CmsAbstractRequestHandler.class);

    protected Map<String, Object> sendDownloadUrl(Map<String, Object> request, String downloadUrl) {

        logger.debug("Send download url request received [{}]", request);

        CmsDownloadUrlHandler downloadUrlHandler = null;
        if(request.containsKey(channelK))
        {
            String channelKey = (String) request.get(channelK);
            logger.debug("Retrieving download handler for ["+channelKey+"]");
            downloadUrlHandler = getDownloadUrlHandlers().get(channelKey);
        } else {
            logger.debug("Retrieving download default download handler");
            downloadUrlHandler = getDownloadUrlHandlers().get(getProperty("cms.default.download.channel"));
        }

        return downloadUrlHandler.handleDownloadRequest(request, downloadUrl);
    }

    protected Map<String, Object> sendCaasRequest(String paymentInstrumentName, Map<String, Object> downloadRequest, Map<String, Object> additionalParams) {
        Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(amountK, downloadRequest.get(cmsChargingAmountK));
        sdpRequest.put(currencyK, getProperty("cms.system.currency"));
        sdpRequest.put(subscriberIdK, downloadRequest.get(addressK));
        sdpRequest.put(paymentInstrumentNameK, paymentInstrumentName);
        sdpRequest.put(externalTrxIdK, downloadRequest.get(idK));

        if(additionalParams != null && !additionalParams.isEmpty()) {
            if(additionalParams.containsKey(mpaisaPaymentRefId)) {
                additionalParams.put(mpaisaPaymentRefId, sdpRequest.get(externalTrxIdK));
            }

            sdpRequest.put(additionalParamsK, additionalParams);
        }

        Map<String, Object> sdpResponse = cmsSdpService().sendCaasRequest(sdpRequest);

        if (successCode.equals(sdpResponse.get(statusCodeK))) {
            downloadRequest.put(statusK, eligibleK);
            downloadRequest.put(chargingStatusCodeK, sdpResponse.get(statusCodeK));
            downloadRequest.put(chargingStatusDescriptionsK, sdpResponse.get(statusDetailK));
            cmsRequestRepositoryService().updateRequest(downloadRequest);

            createDownloadDetail(downloadRequest, (Map<String, Object>) downloadRequest.get(cmsContentInfoK));

            return sendDownloadUrl(downloadRequest, createDownloadUrl(downloadRequest.get(idK)));
        }

        if (paymentPendingPartialSuccessCode.equals(sdpResponse.get(statusCodeK))) {
            Object longDescription = sdpResponse.get(longDescriptionK);

            downloadRequest.put(statusK, chargingRequestedK);
            downloadRequest.put(paymentInstructionsK, longDescription);
            downloadRequest.put(chargingStatusCodeK, sdpResponse.get(statusCodeK));
            downloadRequest.put(chargingStatusDescriptionsK, sdpResponse.get(shortDescriptionK));

            Map<String, Object> response = new HashMap<String, Object>();

            response.put(statusCodeK, cmsWithInstructionPartialSuccessCode);
            response.put(statusDetailK, sdpResponse.get(shortDescriptionK));
            response.put(longDescriptionK, longDescription);
            response.put(shortDescriptionK, sdpResponse.get(shortDescriptionK));

            cmsRequestRepositoryService().updateRequest(downloadRequest);

            return response;
        }

        downloadRequest.put(statusK, chargingFailedK);
        cmsRequestRepositoryService().updateRequest(downloadRequest);

        return sdpResponse;
    }

    protected void createDownloadDetail(Map<String, Object> request, Map<String, Object> contentMap) {
        Map<String, Object> downloadLink = new HashMap<String, Object>();

        String appId = (String) contentMap.get(appIdK);

        Map<String, Object> ncsDetailMap = ncsRepositoryService().findByAppIdOperatorNcsType(appId, "", downloadableK);

        String time = (String) ncsDetailMap.get(linkExpTimeK);
        String timeType = (String) ncsDetailMap.get(linkExpTimeTypeK);

        Date expireDate = getExpireDate(new Date(), timeType, time);

        downloadLink.put(idK, request.get(idK));
        downloadLink.put(appIdK, appId);
        downloadLink.put(buildFileIdK, contentMap.get(buildAppFileIdK));
        downloadLink.put(expireDateK, expireDate);
        downloadLink.put(statusK, initialK);

        cmsInternalRepositoryService().createDownload(downloadLink);
    }

    protected String createDownloadUrl(Object requestId) {
        StringBuilder sb = new StringBuilder();

        sb.append(getProperty("cms.internal.app.wap.push.url.prefix"))
                .append("cms/download-file?id=")
                .append(requestId);

        return sb.toString();
    }
}
