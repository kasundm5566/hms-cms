/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.handlers.query;

import hms.cms.common.CmsKeyBox;
import hms.cms.common.QueryType;
import hms.cms.handlers.CmsAbstractRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.*;
import static hms.kite.util.KiteKeyBox.cmsDevicesK;
import static hms.kite.util.KiteKeyBox.cmsPlatformsK;
import static hms.kite.util.KiteKeyBox.versionK;

/**
 * Created with IntelliJ IDEA.
 * User: sanjeewa
 * Date: 6/27/12
 * Time: 11:51 AM
 * <p/>
 * Handles all the query requests
 */
public class CmsQueryRequestHandler extends CmsAbstractRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(CmsQueryRequestHandler.class);

    private void validationSuccess(Map<String, Object> request, Map<String, Object> response) {
        logger.info("Request is validated successfully {}", request);
        response.put(statusCodeK, cmsSuccessCode);
        response.put(statusDetailK, "Request is validated successfully");
    }

    private void validationFailForAppId(Map<String, Object> request, Map<String, Object> response) {
        logger.error("CMS query request doesn't contain {}... [{}]", applicationIdK, request);
        response.put(statusCodeK, cmsAppIdErrorCode);
        response.put(statusDetailK, "Application ID is not found withing the request");
    }

    private void validationFailForAddress(Map<String, Object> request, Map<String, Object> response) {
        logger.error("CMS query request doesn't contain {}... [{}]", addressK, request);
        response.put(statusCodeK, cmsAddressErrorCode);
        response.put(statusDetailK, "Address is not found withing the request");
    }

    private Map<String, Object> handleDownloadsQuery(Map<String, Object> request) {
        Map<String, Object> response = validateDownloadsQuery(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processDownloadsQuery(request);
        }

        return response;
    }

    private Map<String, Object> validateDownloadsQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(applicationIdK)) {
            validationFailForAppId(request, response);
        } else {
            validationSuccess(request, response);
        }

        return response;
    }

    private Map<String, Object> processDownloadsQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        Map<String, Object> devicesAndPlatformsInfoMap = cmsInfindoRepositoryService().getDownloadSupportInfoByAppId(request);

        if (isInfoCorrect(response, devicesAndPlatformsInfoMap)) return response;

        String appId = (String) request.get(applicationIdK);
        Map<String, Object> devicesPlatformsWiseBuildFiles = getDevicesPlatformWiseBuildFiles(appId);

        if (isInfoCorrect(response, devicesPlatformsWiseBuildFiles)) return response;

        response.put(statusCodeK, cmsContentNotFoundErrorCode);
        response.put(statusDetailK, "There are no approved downloadable files for given Application ID");

        return response;
    }

    private Map<String, Object> processContentDetailsQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        String appId = ((List<String>) request.get(applicationIdListK)).get(0);

        Map<String, Object> devicesPlatformsWiseBuildFiles = getDevicesPlatformWiseBuildFiles(appId);

        if (!devicesPlatformsWiseBuildFiles.isEmpty()) {
            response.put(statusCodeK, successCode);
            response.put(statusDetailK, "Success");
            response.put(resultK, devicesPlatformsWiseBuildFiles);
        } else {
            response.put(statusCodeK, cmsContentNotFoundErrorCode);
            response.put(statusDetailK, "There are no approved downloadable files for search criteria");
        }

        return response;
    }

    private Map<String, Object> getDevicesPlatformWiseBuildFiles(String appId) {
        Map<String, Object> platformBuildFilesMap = new HashMap<>();

        List<Map<String, Object>> byAppId = buildFileRepositoryService().findByAppId(appId);

        for (Map<String, Object> appDetails : byAppId) {
            HashMap<String, String> content = new HashMap<String, String>();
            content.put(cmsContentIdK, (String) appDetails.get(idK));
            content.put(CmsKeyBox.versionK, "");

            String platformName = (String) appDetails.get("platform-name");
            List<Map<String, Object>> platformContents = new ArrayList<>();

            List<String> platformVersions = (List<String>) appDetails.get(platformVersionsK);
            for (String platformVersion : platformVersions) {
                Map<String, Object> buildFileVersion = new HashMap<>();
                buildFileVersion.put(CmsKeyBox.versionK, platformVersion);
                buildFileVersion.put(contentK, Arrays.asList(content));

                platformContents.add(buildFileVersion);
            }

            if(platformBuildFilesMap.get(platformName) != null) {
                ((List<Map<String, Object>>) platformBuildFilesMap.get(platformName)).addAll(platformContents);
            } else {
                platformBuildFilesMap.put(platformName, platformContents);
            }
        }

        Map<String, Object> platformAndDevices = new HashMap<>();
        platformAndDevices.put(cmsPlatformsK, platformBuildFilesMap);
        platformAndDevices.put(cmsDevicesK, Collections.emptyMap());
        Map<String, Object> appWisePlatforms = new HashMap<>();
        appWisePlatforms.put(appId, platformAndDevices);
        return appWisePlatforms;
    }

    private boolean isInfoCorrect(Map<String, Object> response, Map<String, Object> devicesAndPlatformsInfoMap) {
        Map<String, Object> devices = (Map<String, Object>) devicesAndPlatformsInfoMap.get(cmsDevicesK);
        Map<String, Object> platforms = (Map<String, Object>) devicesAndPlatformsInfoMap.get(cmsPlatformsK);

        if (!platforms.isEmpty() || !devices.isEmpty()) {
            logger.debug("Supported devices and platforms information for requested application [{}]", devicesAndPlatformsInfoMap);

            response.put(statusCodeK, successCode);
            response.put(statusDetailK, "Success");
            response.put(resultK, devicesAndPlatformsInfoMap);

            return true;
        }

        return false;
    }

    private Map<String, Object> handleDownloadRequestsQuery(Map<String, Object> request) {
        Map<String, Object> response = validateDownloadRequestsQuery(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processDownloadRequestsQuery(request);
        }

        return response;
    }

    private Map<String, Object> validateDownloadRequestsQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(addressK)) {
            validationFailForAddress(request, response);
        } else {
            validationSuccess(request, response);
        }

        return response;
    }

    private Map<String, Object> processDownloadRequestsQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        List<Map<String, Object>> downloadRequests = cmsRequestRepositoryService().findByAddress((String) request.get(addressK));

        if (downloadRequests.isEmpty()) {
            response.put(statusCodeK, cmsDownloadRequestNotFoundErrorCode);
            response.put(statusDetailK, "No download request found for given address");
            return response;
        }

        response.put(statusCodeK, cmsSuccessCode);
        response.put(statusDetailK, "Success");
        response.put(resultK, downloadRequests);

        return response;
    }

    private Map<String, Object> handleDownloadRequestStatusQuery(Map<String, Object> request) {
        Map<String, Object> response = validateDownloadRequestStatusQuery(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processDownloadRequestStatusQuery(request);
        }

        return response;
    }

    private Map<String, Object> validateDownloadRequestStatusQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(applicationIdK)) {
            validationFailForAppId(request, response);
        } else if (!request.containsKey(addressK)) {
            validationFailForAddress(request, response);
        } else {
            validationSuccess(request, response);
        }

        return response;
    }

    private Map<String, Object> processDownloadRequestStatusQuery(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        String appId = (String) request.get(applicationIdK);
        String address = (String) request.get(addressK);

        Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByAppIdAndAddress(appId, address);

        if (downloadRequest.isEmpty()) {
            response.put(statusCodeK, cmsDownloadRequestNotFoundErrorCode);
            response.put(statusDetailK, "No download request found for given application Id and address");
            return response;
        }

        response.put(statusCodeK, cmsSuccessCode);
        response.put(statusDetailK, "Success");
        response.put(resultK, downloadRequest);

        return response;
    }

    @Override
    public Map<String, Object> handleRequest(Map<String, Object> request) {
        QueryType type = (QueryType) request.get(queryTypeK);

        switch (type) {
            case DOWNLOADS:
                return handleDownloadsQuery(request);
            case CONTENT_DETAILS:
                return   processContentDetailsQuery(request);
            case DOWNLOAD_REQUESTS:
                return handleDownloadRequestsQuery(request);
            case DOWNLOAD_REQUEST_STATUS:
                return handleDownloadRequestStatusQuery(request);
            default:
                logger.error("Unknown query request received [{}]", type);
                throw new IllegalArgumentException("Unknown query request received");
        }
    }
}
