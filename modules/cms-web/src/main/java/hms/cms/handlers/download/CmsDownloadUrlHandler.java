/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.cms.handlers.download;

import hms.cms.service.DownloadUrlSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;
import static hms.kite.util.KiteKeyBox.statusK;

/**
 * <p>
 * CMS Download url sender.
 * </p>
 *
 * @author manuja
 */
public class CmsDownloadUrlHandler {

    private static final Logger logger = LoggerFactory.getLogger(CmsDownloadUrlHandler.class);

    private DownloadUrlSender downloadUrlSender;

    /**
     * <p>
     * Constructor of CmsDownloadUrlHandler.
     * </p>
     *
     * @param downloadUrlSender download url sender instance
     */
    public CmsDownloadUrlHandler(final DownloadUrlSender downloadUrlSender) {
        this.downloadUrlSender = downloadUrlSender;
    }

    /**
     * <p>
     *     Handle download request.
     * </p>
     *
     * @param request request context
     * @param downloadUrl download url
     *
     * @return response message to be sent
     */
    public Map<String, Object> handleDownloadRequest(Map<String, Object> request, String downloadUrl) {

        logger.debug("Download url [{}].", downloadUrl);

        Map<String, Object> sdpResponse = downloadUrlSender.sendDownloadUrl(request, downloadUrl);
        Map<String, Object> response = new HashMap<String, Object>();
        if (cmsSuccessCode.equals(sdpResponse.get(statusCodeK))) {
            response.put(statusCodeK, cmsPartialSuccessCode);
            response.put(statusDetailK, "You will receive a message shortly");
            response.put(wapUrlNblK, downloadUrl);

            request.put(chargingStatusCodeK, response.get(statusCodeK));
            request.put(chargingStatusDescriptionsK, response.get(statusDetailK));
            request.put(statusK, wapPushDeliveredK);

            cmsRequestRepositoryService().updateRequest(request);
        } else {
            Map<String, Object> failResponse = new HashMap<String, Object>();
            failResponse.put(statusCodeK, internalErrorErrorCode);
            failResponse.put(statusDetailK, "Internal error occurred");
            return failResponse;
        }
        return response;
    }
}
