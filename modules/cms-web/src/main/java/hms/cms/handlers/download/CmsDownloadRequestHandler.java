/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.handlers.download;

import com.google.common.collect.Maps;
import hms.cms.handlers.CmsAbstractRequestHandler;
import hms.cms.util.OperatorCheckingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.*;
import static hms.cms.service.CmsServiceRegistry.cmsSdpService;
import static hms.cms.service.CmsServiceRegistry.getProperty;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Handles all the download requests
 */
public class CmsDownloadRequestHandler extends CmsAbstractRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(CmsDownloadRequestHandler.class);
    private Long lastContentId;
    private OperatorCheckingService operatorCheckingService;

    private Map<String, Object> processDownload(Map<String, Object> request) {
        String contentId = (String) request.get(contentIdK);

        Map<String, Object> contentMap = cmsInfindoRepositoryService().getDownloadDetailsByContentId(contentId);

        if (contentMap != null) {
            return processInfindoDownload(request, contentMap);
        }

        contentMap = buildFileRepositoryService().findByBuildFileId(contentId);

        if (contentMap == null) {
            logger.debug("No content found for [{}]", request);

            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusCodeK, cmsContentNotFoundErrorCode);
            response.put(statusDetailK, "No content found for given Content ID");

            return response;
        }

        setLastContentId(request, contentMap);
        return processInternalDownload(request, contentMap);
    }

    private Map<String, Object> processInfindoDownload(Map<String, Object> request, Map<String, Object> contentMap) {
        logger.debug("Infindo application download [{}]", contentMap);

        request.put(cmsRepositoryK, infindoDownloadK);
        request.put(cmsChargingTypeK, freeK);
        request.put(statusK, eligibleK);

        saveRequest(request, contentMap);

        return sendDownloadUrl(request, (String) contentMap.get(wapUrlNblK));
    }

    private void saveRequest(Map<String, Object> request, Map<String, Object> contentMap) {
        request.put(cmsContentInfoK, contentMap);
        request.put(operatorK, operatorCheckingService.findOperator((String)request.get(addressK)).get());

        logger.debug("Saving request in DB [{}]", request);
        cmsRequestRepositoryService().createRequest(request);
    }

    private void setLastContentId(Map<String, Object> request, Map<String, Object> contentMap){
        String appId = (String) contentMap.get(appIdK);
        String msisdn = (String) request.get(addressK);
        lastContentId = Long.MAX_VALUE;

        Map<String, Object> lastBuildFile =  cmsRequestRepositoryService().findByAppIdAndAddress(appId, msisdn);

        if(!lastBuildFile.isEmpty()){
            Map<String, Object> detailsOfLastBuildFile =  cmsRequestRepositoryService().findByRequestId((String) lastBuildFile.get(idK));
            lastContentId = Long.parseLong((String) detailsOfLastBuildFile.get(contentIdK));

            logger.debug("Last content Id of downloaded APK of [{}] is [{}]", appId, lastContentId);
        } else {
            logger.debug("Downloading [{}] application for the first time", appId);
        }
    }

    private Map<String, Object> processInternalDownload(Map<String, Object> request, Map<String, Object> contentMap) {
        logger.debug("Process internal download [{}]", contentMap);

        request.put(cmsRepositoryK, internalDownloadK);

        String appId = (String) contentMap.get(appIdK);

        Map<String, Object> ncsDetailMap = ncsRepositoryService().findByAppIdOperatorNcsType(appId, "", downloadableK);

        Long requestingContentId = Long.parseLong((String) request.get(contentIdK));

        logger.debug("Requesting content Id of [{}] is [{}]", appId, requestingContentId);
        logger.debug("Downloadable NCS SLA for [{}] [{}]", appId, ncsDetailMap);

        Map<String, Object> chargingDetails = (Map<String, Object>) ncsDetailMap.get(chargingK);

        Object chargingType = chargingDetails.get(typeK);
        Object chargingAmount = chargingDetails.get(amountK);
        List<String> ncsSlaAllowedPis = (List<String>) chargingDetails.get(allowedPaymentInstrumentsK);

        request.put(cmsChargingTypeK, chargingType);
        request.put(cmsChargingAmountK, chargingAmount);

        if (freeK.equals(chargingType)) {
            return processInternalDownloadWithFreeCharging(request, contentMap);
        } else if (flatK.equals(chargingType) && lastContentId < requestingContentId) {
//            Charging is not applicable for app updates
            logger.debug("Downloading an update of [{}] for FREE", appId);
            return processInternalDownloadWithFreeCharging(request, contentMap);
        } else if (flatK.equals(chargingType)) {
            return processInternalDownloadWithCharging(request, contentMap, ncsSlaAllowedPis);
        } else {
            logger.error("Charging type unknown for NCS SLA [{}]", ncsDetailMap);
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusCodeK, cmsChargingUnknownTypeErrorCode);
            response.put(statusDetailK, "Unknown charging type found [" + chargingType + "]");
            return response;
        }
    }

    private Map<String, Object> processInternalDownloadWithFreeCharging(Map<String, Object> request, Map<String, Object> contentMap) {
        logger.debug("Internal downloadable application download with free charging [{}]", contentMap);

        request.put(statusK, eligibleK);
        saveRequest(request, contentMap);

        logger.debug("Create download detail for internal download request : [{}} with [{}] download content", request, contentMap);

        createDownloadDetail(request, contentMap);

        return sendDownloadUrl(request, createDownloadUrl(request.get(idK)));
    }

    private Map<String, Object> processInternalDownloadWithCharging(Map<String, Object> request, Map<String, Object> contentMap, List<String> ncsSlaAllowedPis) {
        logger.debug("Internal downloadable application download with charging [{}]", contentMap);

        Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(subscriberIdK, request.get(addressK));
        sdpRequest.put(typeK, allK);

        Map<String, Object> sdpResponse = cmsSdpService().getPiList(sdpRequest);

        Map<String, Object> response = new HashMap<String, Object>();

        if (!cmsSuccessCode.equals(sdpResponse.get(statusCodeK))) {
            return sdpResponse;
        }

        List<Map> cmsAppAllowedPis = (List<Map>) sdpResponse.get(paymentInstrumentListK);

        List<Map> filteredPis = filterPiList(ncsSlaAllowedPis, cmsAppAllowedPis);

        if (filteredPis == null || filteredPis.isEmpty()) {
            response.put(statusCodeK, cmsSystemErrorErrorCode);
            response.put(statusDetailK, "Internal error occurred");

            return response;
        }

        if (filteredPis.size() == 1) {
            request.put(statusK, chargingPendingK);
            String paymentInstrumentName = (String) filteredPis.get(0).get(nameK);
            request.put(paymentInstrumentNameK, paymentInstrumentName);
            request.put(currencyK, getProperty("cms.system.currency"));

            saveRequest(request, contentMap);
            response = sendCaasRequest(paymentInstrumentName, request, Maps.<String, Object>newHashMap());
        } else {
            response.put(statusCodeK, cmsWithPiListPartialSuccessCode);
            response.put(statusDetailK, "Select a payment instrument to do the charging");
            response.put(paymentInstrumentListK, filteredPis);
            response.put(downloadRequestIdK, request.get(idK));

            request.put(chargingStatusCodeK, response.get(statusCodeK));
            request.put(chargingStatusDescriptionsK, response.get(statusDetailK));
            request.put(statusK, acceptedK);
            saveRequest(request, contentMap);
        }

        return response;
    }

    private List<Map> filterPiList(List<String> ncsSlaAllowedPis, List<Map> cmsAppPiList) {
        List<Map> filteredNcsAllowedPis = new ArrayList<>();

        for (Map cmsPi : cmsAppPiList) {
            String cmsPiName = ((String) cmsPi.get(nameK)).split("/")[0];
            if (ncsSlaAllowedPis.contains(cmsPiName)) {
                filteredNcsAllowedPis.add(cmsPi);
            }
        }

        return filteredNcsAllowedPis;
    }

    private Map<String, Object> validateRequest(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(applicationIdK)) {
            logger.error("CMS download request doesn't contain {}... [{}]", applicationIdK, request);
            response.put(statusCodeK, cmsAppIdErrorCode);
            response.put(statusDetailK, "Application ID is not found withing the request");
        } else if (!request.containsKey(contentIdK)) {
            logger.error("CMS download request doesn't contain {}... [{}]", contentIdK, request);
            response.put(statusCodeK, cmsContentIdErrorCode);
            response.put(statusDetailK, "Content ID is not found withing the request");
        } else if (!request.containsKey(addressK)) {
            logger.error("CMS download request doesn't contain {}... [{}]", addressK, request);
            response.put(statusCodeK, cmsRecipientAddressErrorCode);
            response.put(statusDetailK, "Receiver address is not found withing the request");
        } else {
            logger.info("Request is validated successfully {}", request);
            response.put(statusCodeK, cmsSuccessCode);
            response.put(statusDetailK, "Request is validated successfully");
        }

        return response;
    }

    @Override
    public Map<String, Object> handleRequest(Map<String, Object> request) {
        Map<String, Object> response = validateRequest(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processDownload(request);
        }

        return response;
    }

    public void setOperatorCheckingService(OperatorCheckingService operatorCheckingService) {
        this.operatorCheckingService = operatorCheckingService;
    }
}
