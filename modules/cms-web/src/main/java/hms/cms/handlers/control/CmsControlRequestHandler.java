/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.handlers.control;

import hms.cms.handlers.CmsAbstractRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;
import static hms.cms.service.CmsServiceRegistry.getProperty;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Handles all control requests
 */
public class CmsControlRequestHandler extends CmsAbstractRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(CmsControlRequestHandler.class);

    private Map<String, Object> processResendWapPushRequest(Map<String, Object> request) {
        Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByRequestId((String) request.get(downloadRequestIdK));

        if (downloadRequest == null) {
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusCodeK, cmsDownloadRequestNotFoundErrorCode);
            response.put(statusDetailK, "No download request found for given Download Request ID");

            return response;
        }

        logger.debug("Download request content for resend wap push request [{}]", downloadRequest);

        logger.debug("Saving control request in DB [{}]", request);
        cmsRequestRepositoryService().createRequest(request);

        downloadRequest.put(statusK, eligibleK);
        cmsRequestRepositoryService().updateRequest(downloadRequest);

        if (infindoDownloadK.equals(downloadRequest.get(cmsRepositoryK))) {
            return processInfindoDownloadRequest(downloadRequest);
        }

        return processInternalDownloadRequest(downloadRequest);
    }

    private Map<String, Object> processInfindoDownloadRequest(Map<String, Object> request) {
        Map contentMap = (Map) request.get(cmsContentInfoK);

        return sendDownloadUrl(request, (String) contentMap.get(wapUrlNblK));
    }

    private Map<String, Object> processInternalDownloadRequest(Map<String, Object> request) {
        Map<String, Object> contentMap = (Map<String, Object>) request.get(cmsContentInfoK);

        createDownloadDetail(request, contentMap);

        return sendDownloadUrl(request, createDownloadUrl(request.get(idK)));
    }

    private Map<String, Object> validateRequest(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(downloadRequestIdK)) {
            logger.error("CMS resend wap push request doesn't contain {}... [{}]", downloadRequestIdK, request);
            response.put(statusCodeK, cmsDownloadRequestIdErrorCode);
            response.put(statusDetailK, "Download Request ID is not found withing the request");
        } else {
            logger.info("Request is validated successfully {}", request);
            response.put(statusCodeK, cmsSuccessCode);
            response.put(statusDetailK, "Request is validated successfully");
        }

        return response;
    }

    @Override
    public Map<String, Object> handleRequest(Map<String, Object> request) {
        Map<String, Object> response = validateRequest(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processResendWapPushRequest(request);
        }

        return response;
    }
}
