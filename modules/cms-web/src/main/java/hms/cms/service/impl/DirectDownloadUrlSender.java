/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.cms.service.impl;

import hms.cms.service.DownloadUrlSender;

import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.cmsSuccessCode;
import static hms.cms.common.CmsKeyBox.statusCodeK;

/**
 * <p>
 * Direct download url sender implementation.
 * </p>
 *
 * @author Manuja
 */
public class DirectDownloadUrlSender implements DownloadUrlSender {

    @Override
    public Map<String, Object> sendDownloadUrl(Map<String, Object> request, String downloadUrl) {
        Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, cmsSuccessCode);
        return sdpResponse;
    }
}
