/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.cms.service;

import java.util.Map;

/**
 * <p>
 * Download URL Sender interface. This will be implemented for difference NCS Types which used to send download URL.
 * </p>
 *
 * @author Manuja
 */
public interface DownloadUrlSender {

    /**
     * <p>
     * Send download url via implemented ncs type.
     * </p>
     *
     * @param request request context
     * @param downloadUrl download url
     *
     * @return  response received from sdp
     */
    Map<String, Object> sendDownloadUrl(Map<String, Object> request, String downloadUrl);
}

