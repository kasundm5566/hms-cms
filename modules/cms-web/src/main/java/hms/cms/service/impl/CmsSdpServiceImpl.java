/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.service.impl;

import hms.cms.common.Http;
import hms.cms.service.CmsSdpService;
import hms.cms.service.CmsServiceRegistry;
import hms.commons.SnmpLogUtil;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.service.CmsServiceRegistry.getProperty;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsSdpServiceImpl implements CmsSdpService {

    private static final Logger logger = LoggerFactory.getLogger(CmsSdpServiceImpl.class);

    private Map<String, Object> sendSdpRequest(Map<String, Object> request, String url) {
        logger.debug("SDP Request [{}]", request);

        Map<String, Object> response;

        try {
            WebClient webClient = Http.createWebClient(CmsServiceRegistry.getProperty(url));
            response = webClient.post(request, HashMap.class);

            logger.debug("SDP Response [{}]", response);

            SnmpLogUtil.clearTrap("cmsToSdpConnection", CmsServiceRegistry.getProperty("cms.to.sdp.connection.success.snmp.message"));
        } catch (Exception e) {
            logger.error("Error occurred while sending the SDP request [{}]", e);

            SnmpLogUtil.trap("cmsToSdpConnection", CmsServiceRegistry.getProperty("cms.to.sdp.connection.fail.snmp.message"));

            response = new HashMap<String, Object>();

            response.put(statusCodeK, cmsSystemErrorErrorCode);
            response.put(statusDetailK, "Something went wrong when sending the SDP request");
        }

        return response;
    }

    @Override
    public Map<String, Object> sendWapPush(Map<String, Object> sdpRequest) {
        sdpRequest.put(versionK, "1.0");
        sdpRequest.put(applicationIdK, getProperty("cms.system.app.id"));
        sdpRequest.put(passwordK, getProperty("cms.system.app.password"));
        sdpRequest.put(wapPushTypeNblK, getProperty("sdp.wap.push.type"));
        sdpRequest.put(statusRequestK, getProperty("sdp.wap.push.default.request"));

        return sendSdpRequest(sdpRequest, "sdp.nbl.wap.push.url");
    }

    @Override
    public Map<String, Object> sendSms(Map<String, Object> sdpRequest) {
        sdpRequest.put(versionK, "1.0");
        sdpRequest.put(applicationIdK, getProperty("cms.system.app.id"));
        sdpRequest.put(passwordK, getProperty("cms.system.app.password"));

        return sendSdpRequest(sdpRequest, "sdp.nbl.sms.url");
    }

    @Override
    public Map<String, Object> getPiList(Map<String, Object> sdpRequest) {
        sdpRequest.put(applicationIdK, getProperty("cms.system.app.id"));
        sdpRequest.put(passwordK, getProperty("cms.system.app.password"));

        return sendSdpRequest(sdpRequest, "sdp.nbl.caas.pi.list.url");
    }

    @Override
    public Map<String, Object> sendCaasRequest(Map<String, Object> sdpRequest) {
        sdpRequest.put(applicationIdK, getProperty("cms.system.app.id"));
        sdpRequest.put(passwordK, getProperty("cms.system.app.password"));

        return sendSdpRequest(sdpRequest, "sdp.nbl.caas.direct.debit.url");
    }
}
