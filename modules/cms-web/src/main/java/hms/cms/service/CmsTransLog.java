package hms.cms.service;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import hms.cms.common.CmsKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hms.cms.common.CmsKeyBox.cmsPartialSuccessCode;
import static hms.cms.common.CmsKeyBox.successCode;
import static hms.kite.util.KiteKeyBox.statusK;
import static hms.kite.util.KiteKeyBox.telK;

public class CmsTransLog {

   private static final List<String> loggingEvent = Lists.newArrayList( CmsKeyBox.acceptedK , CmsKeyBox.chargingPendingK, CmsKeyBox.eligibleK, CmsKeyBox.wapPushDeliveredK, CmsKeyBox.chargingFailedK, CmsKeyBox.downloadCompletedK);

   public enum CmsTransLogField {

        CORRELATION_ID("_id", "", noModificationToValue()),
        DATE_TIME("created-date", "", transformToSdpDateFormat()),
        APP_ID("content-info.app-id", "", noModificationToValue()),
        SP_ID("content-info.sp-id", "", noModificationToValue()),
        NCS("ncs", KiteKeyBox.downloadableK, noModificationToValue()),
        CONTENT_ID("contentId", "", noModificationToValue()),
        MOBILE_NO("address", "", transformToSdpTransLogMsisdnFormat()),
        REQUEST_TYPE("request-type", "", noModificationToValue()),
        DIRECTION("direction", "ao", noModificationToValue()),
        REQUEST_STATUS("status", "", noModificationToValue()),
        CHARGING_TYPE("charging-type", "", noModificationToValue()),
        PAYMENT_INSTRUMENT("paymentInstrumentName", "", noModificationToValue()),
        CHARGING_AMOUNT("charging-amount", "", noModificationToValue()),
        CURRENCY_CODE("currency", "", noModificationToValue()),
        STATUS_CODE("charging-status-code", "", noModificationToValue()),
        STATUS_DESCRIPTION("charging-status-code", "", transformToDataLoaderStatusCode()),
        PLATFORM("platform-id", "", noModificationToValue()),
        PLATFORM_VERSION("platform-versions", "", noModificationToValue()),
        DEVICE_MAKE("device-make", "", noModificationToValue()),
        DEVICE_MODEL("device-model", "", noModificationToValue()),
        OPERATOR("operator", "", noModificationToValue());

       private static final Pattern pattern = Pattern.compile("^" + telK + ":(?<msisdn>[\\d]+)" + "$");

       private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");

       private static final Map dataLoaderCmsStatusDescription =
               ImmutableMap.builder().
                       put(successCode, "success").
                       put(cmsPartialSuccessCode, "success").
                       build();

       private static final Function<Object, Object> transformToDataLoaderStatusCode() {
           return new Function<Object, Object>() {
                @Override
                public Object apply(Object input) {
                    final Optional<Object> description = Optional.fromNullable(dataLoaderCmsStatusDescription.get(input));
                    if(description.isPresent()) {
                        return description.get();
                    } else {
                        return "failure";
                    }
                }
            };
       }

       private static final <T> Function<T, T> noModificationToValue() {
           return new Function<T, T>() {
               @Override
               public T apply(T input) {
                   return input;
               }
           };
       }

       private static final Function<String, String> transformToSdpTransLogMsisdnFormat() {
           return new Function<String, String>() {
               @Override
               public String apply(String input) {
                   Matcher matcher = pattern.matcher(input.trim());
                   matcher.matches();
                   return matcher.group("msisdn");
               }
           };
       }

       private static final <T> Function<T, String> transformToSdpDateFormat() {
           return new Function<T, String>() {
               @Override
               public String apply(T input) {
                   return DATE_FORMAT.format((Date)input) ;
               }
           };
       }

       private String logItemName;
       private String defaultValue;
       private Function function;

       CmsTransLogField(String id, String defaultValue, Function function) {
            this.logItemName = id;
            this.defaultValue = defaultValue;
            this.function = function;
       }

       public String logItemName() {
           return logItemName;
       }

       public String defaultValue() {
           return defaultValue;
       }

       public Function getValueTransformFunction() {
           return function;
       }
   }

    private static final Logger transLogger = LoggerFactory.getLogger("trans_logger");

    private static final StringBuffer logPattern = new StringBuffer("{}");

    static {
        for(int i = 1; i < CmsTransLogField.values().length; i++) {
            logPattern.append("|").append("{}");
        }
    }

    public static void log(Map<String, Object> logData) {

        try {
            if(loggingEvent.contains(logData.get(statusK))) {
                Map<String, Object> transformedMap = transformToFlatMap(logData, "");

                Object[] logObjects = new Object[CmsTransLogField.values().length];

                for (int i = 0; i < CmsTransLogField.values().length; i++) {
                    final CmsTransLogField cmsTransLogField = CmsTransLogField.values()[i];
                    final Object item = transformedMap.get(cmsTransLogField.logItemName());
                    logObjects[i] =  item != null ? cmsTransLogField.getValueTransformFunction().apply(item) : cmsTransLogField.defaultValue();
                }

                transLogger.info(logPattern.toString(), logObjects);
            }
        } catch (NullPointerException | ClassCastException e) {
            return;
        }
    }

    public static Map<String, Object> transformToFlatMap(Map<String, Object> map, String hierarchy) {
        Map<String, Object> transformedMap = new HashMap<String, Object>();
        for (final String key : map.keySet()) {
            if(map.get(key) instanceof Map) {
                @SuppressWarnings(value = "unchecked")
                Map<String, Object> subMap =
                        transformToFlatMap((Map<String, Object>) map.get(key), hierarchy + key + ".");
                transformedMap.putAll(subMap);
            } else {
                transformedMap.put(hierarchy + key, map.get(key));
            }
        }
        return transformedMap;
    }

}
