/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.service;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This contains SDP service calls
 */
public interface CmsSdpService {

    /**
     * Try to deliver wap push message reliably. Retry for configured time and send success or failure.
     *
     * @param sdpRequest
     * @return
     */
    Map<String, Object> sendWapPush(Map<String, Object> sdpRequest);

    /**
     * Try to deliver sms message reliably. Retry for configured time and send success or failure.
     *
     * @param sdpRequest
     * @return
     */
    Map<String, Object> sendSms(Map<String, Object> sdpRequest);


    /**
     * Get payment instrument list from SDP
     *
     * @param sdpRequest
     * @return
     */
    Map<String, Object> getPiList(Map<String, Object> sdpRequest);

    /**
     * Send CAAS Request to SDP
     *
     * @param sdpRequest
     * @return
     */
    Map<String, Object> sendCaasRequest(Map<String, Object> sdpRequest);
}
