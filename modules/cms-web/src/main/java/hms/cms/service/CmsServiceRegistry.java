/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.cms.service;

import hms.cms.handlers.download.CmsDownloadUrlHandler;

import java.util.Map;
import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public final class CmsServiceRegistry {

    private static Properties cmsProperties;
    private static CmsSdpService cmsSdpService;
    private static CmsService cmsService;
    private static CmsEventLog cmsEventLog;
    private static Map<String, CmsDownloadUrlHandler> downloadUrlHandlers;

    static {
        setCmsProperties(new NullObject<Properties>().get());
        setCmsSdpService(new NullObject<CmsSdpService>().get());
        setCmsService(new NullObject<CmsService>().get());
        setCmsEventLog(new NullObject<CmsEventLog>().get());
        setDownloadUrlHandlers(new NullObject<Map<String, CmsDownloadUrlHandler>>().get());
    }

    public static String getProperty(String key) {
        return cmsProperties.getProperty(key);
    }

    private static void setCmsProperties(Properties cmsProperties) {
        CmsServiceRegistry.cmsProperties = cmsProperties;
    }

    public static CmsSdpService cmsSdpService() {
        return cmsSdpService;
    }

    private static void setCmsSdpService(CmsSdpService cmsSdpService) {
        CmsServiceRegistry.cmsSdpService = cmsSdpService;
    }

    public static CmsService cmsService() {
        return cmsService;
    }

    private static void setCmsService(CmsService cmsService) {
        CmsServiceRegistry.cmsService = cmsService;
    }

    public static CmsEventLog cmsEventLog() {
        return cmsEventLog;
    }

    private static void setCmsEventLog(CmsEventLog cmsEventLog) {
        CmsServiceRegistry.cmsEventLog = cmsEventLog;
    }

    public static Map<String, CmsDownloadUrlHandler> getDownloadUrlHandlers() {
        return downloadUrlHandlers;
    }

    private static void setDownloadUrlHandlers(Map<String, CmsDownloadUrlHandler> downloadUrlHandlers) {
        CmsServiceRegistry.downloadUrlHandlers = downloadUrlHandlers;
    }

    private static class NullObject<T> {

        public T get() {
            return null;
        }
    }
}
