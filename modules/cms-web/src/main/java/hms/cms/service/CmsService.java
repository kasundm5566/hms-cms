/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.service;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This class contains all the Cms Services
 */
public interface CmsService {

    /**
     * @param request
     * @return
     */
    Map<String, Object> download(Map<String, Object> request);

    /**
     * @param request
     * @return
     */
    Map<String, Object> chargeForDownload(Map<String, Object> request);

    /**
     * @param request
     * @return
     */
    Map<String, Object> processChargingNotificationForDownload(Map<String, Object> request);

    /**
     * @param request
     * @return
     */
    Map<String, Object> queryForDownloads(Map<String, Object> request);

    /**
     * @param request
     * @return
     */
    Map<String, Object> queryForDownloadRequests(Map<String, Object> request);

    /**
     * @param request
     * @return
     */
    Map<String, Object> queryForDownloadRequestStatus(Map<String, Object> request);

    /**
     * @param request
     * @return
     */
    Map<String, Object> resendWapPush(Map<String, Object> request);

    /**
     * Request used to query for content details stored in CMS
     * @param request
     * @return
     */
    Map<String, Object> queryForContentDetails(Map<String, Object> request);
}
