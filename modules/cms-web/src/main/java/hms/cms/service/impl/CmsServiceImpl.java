/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.service.impl;

import hms.cms.handlers.CmsRequestHandler;
import hms.cms.service.CmsService;
import hms.kite.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.common.QueryType.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This class implements all the Cms Service calls
 */
public class CmsServiceImpl implements CmsService {

    private static final Logger logger = LoggerFactory.getLogger(CmsServiceImpl.class);

    private CmsRequestHandler cmsDownloadRequestHandler;
    private CmsRequestHandler cmsDownloadChargeRequestHandler;
    private CmsRequestHandler cmsChargingNotificationRequestHandler;
    private CmsRequestHandler cmsQueryRequestHandler;
    private CmsRequestHandler cmsControlRequestHandler;

    private String addInfoToRequest(Map<String, Object> request, String requestType, String status) {
        String correlationId = Long.toString(SystemUtil.getCorrelationId());

        request.put(idK, correlationId);
        request.put(cmsRequestTypeK, requestType);
        request.put(statusK, status);

        return correlationId;
    }

    private void log(String correlationId, String message, Map<String, Object> map) {
        logger.debug("Request ID [{}] - " + message + " - [{}]", correlationId, map);
    }

    @Override
    public Map<String, Object> download(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, downloadRequestK, initialK);

        log(correlationId, "CMS Download Request", request);

        Map<String, Object> response = cmsDownloadRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Download Request", response);

        return response;
    }

    @Override
    public Map<String, Object> chargeForDownload(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, chargeRequestK, initialK);

        log(correlationId, "CMS Charge Request", request);

        Map<String, Object> response = cmsDownloadChargeRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Charge Request", response);

        return response;
    }

    @Override
    public Map<String, Object> processChargingNotificationForDownload(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, chargingNotificationRequestK, initialK);

        log(correlationId, "CMS Charging Notification Request", request);

        Map<String, Object> response = cmsChargingNotificationRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Charging Notification Request", response);

        return response;
    }

    @Override
    public Map<String, Object> queryForDownloads(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, queryRequestK, initialK);

        request.put(queryTypeK, DOWNLOADS);

        log(correlationId, "CMS Downloads Query Request", request);

        Map<String, Object> response = cmsQueryRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Downloads Query", response);

        return response;
    }

    @Override
    public Map<String, Object> queryForDownloadRequests(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, queryRequestK, initialK);

        request.put(queryTypeK, DOWNLOAD_REQUESTS);

        log(correlationId, "CMS Download Requests Query Request", request);

        Map<String, Object> response = cmsQueryRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Download Requests Query", response);

        return response;
    }

    @Override
    public Map<String, Object> queryForDownloadRequestStatus(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, queryRequestK, initialK);

        request.put(queryTypeK, DOWNLOAD_REQUEST_STATUS);

        log(correlationId, "CMS Download Request Status Query Request", request);

        Map<String, Object> response = cmsQueryRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Download Request Status Query", response);

        return response;
    }

    @Override
    public Map<String, Object> resendWapPush(Map<String, Object> request) {
        String correlationId = addInfoToRequest(request, controlRequestK, initialK);

        log(correlationId, "CMS Resend Wap Push Control Request", request);

        Map<String, Object> response = cmsControlRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Resend Wap Push Control Request", response);

        return response;
    }

    @Override
    public Map<String, Object> queryForContentDetails(Map<String, Object> request){
        String correlationId = addInfoToRequest(request, queryRequestK, initialK);

        request.put(queryTypeK, CONTENT_DETAILS);

        log(correlationId, "CMS Content Details Query Request", request);

        Map<String, Object> response = cmsQueryRequestHandler.handleRequest(request);

        log(correlationId, "Response for CMS Content Details Query", response);

        return response;
    }


    public void setCmsDownloadRequestHandler(CmsRequestHandler cmsDownloadRequestHandler) {
        this.cmsDownloadRequestHandler = cmsDownloadRequestHandler;
    }

    public void setCmsDownloadChargeRequestHandler(CmsRequestHandler cmsDownloadChargeRequestHandler) {
        this.cmsDownloadChargeRequestHandler = cmsDownloadChargeRequestHandler;
    }

    public void setCmsChargingNotificationRequestHandler(CmsRequestHandler cmsChargingNotificationRequestHandler) {
        this.cmsChargingNotificationRequestHandler = cmsChargingNotificationRequestHandler;
    }

    public void setCmsQueryRequestHandler(CmsRequestHandler cmsQueryRequestHandler) {
        this.cmsQueryRequestHandler = cmsQueryRequestHandler;
    }

    public void setCmsControlRequestHandler(CmsRequestHandler cmsControlRequestHandler) {
        this.cmsControlRequestHandler = cmsControlRequestHandler;
    }
}
