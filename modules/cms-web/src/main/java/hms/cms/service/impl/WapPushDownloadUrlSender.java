package hms.cms.service.impl;

import hms.cms.service.DownloadUrlSender;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.service.CmsServiceRegistry.cmsSdpService;
import static hms.cms.service.CmsServiceRegistry.getProperty;
import static hms.kite.util.KiteKeyBox.addressK;

/**
 * <p>
 *     WapPush Download URL Sender.
 * </p>
 *
 * @author Manuja
 */
public class WapPushDownloadUrlSender implements DownloadUrlSender {

    private String message;

    @Override
    public Map<String, Object> sendDownloadUrl(Map<String, Object> request, String downloadUrl) {
        Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(wapUrlNblK, downloadUrl);
        sdpRequest.put(messageK, message);
        sdpRequest.put(destinationAddressesK, Arrays.asList(request.get(addressK)));
        return cmsSdpService().sendWapPush(sdpRequest);
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
