/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.service.impl;

import hms.cms.service.CmsEventLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsEventLogImpl implements CmsEventLog {

    private static final Logger eventLogger = LoggerFactory.getLogger("event_logger");

    @Override
    public void event(String eventName, Map<String, Object> eventMap) {
        eventLogger.info("{}|{}", eventName, eventMap);
    }
}
