/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.boot;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Collections;
import java.util.Map;

import static hms.cms.service.CmsServiceRegistry.cmsEventLog;
import static hms.cms.service.CmsServiceRegistry.cmsService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/cms-service/")
public class CmsServiceEndPoint {

    @POST
    @Path("/download")
    public Map<String, Object> downloadService(Map<String, Object> request) {

        return cmsService().download(request);
    }

    @POST
    @Path("/download/charge")
    public Map<String, Object> chargeForDownloadService(Map<String, Object> request) {

        return cmsService().chargeForDownload(request);
    }

    @POST
    @Path("/download/charging-notification")
    public Map<String, Object> chargingNotificationForDownloadService(Map<String, Object> request) {

        return cmsService().processChargingNotificationForDownload(request);
    }

    @POST
    @Path("/query/downloads")
    public Map<String, Object> downloadsQueryService(Map<String, Object> request) {

        return cmsService().queryForDownloads(request);
    }

    @POST
    @Path("/query/download-requests")
    public Map<String, Object> downloadRequestsQueryService(Map<String, Object> request) {

        return cmsService().queryForDownloadRequests(request);
    }

    @POST
    @Path("/query/download-request-status")
    public Map<String, Object> downloadRequestStatusQueryService(Map<String, Object> request) {

        return cmsService().queryForDownloadRequestStatus(request);
    }

    @POST
    @Path("/query/content-details")
    public Map<String, Object> contentDetailQueryService(Map<String, Object> request){
        return cmsService().queryForContentDetails(request);
    }

    @POST
    @Path("/control/resend-wap-push")
    public Map<String, Object> resendWapPushService(Map<String, Object> request) {

        return cmsService().resendWapPush(request);
    }
}