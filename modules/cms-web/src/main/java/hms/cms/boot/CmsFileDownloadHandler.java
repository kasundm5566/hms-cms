/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.boot;

import com.mongodb.gridfs.GridFSDBFile;
import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.common.Utils.getThrottlingId;
import static hms.cms.repo.CmsRepositoryServiceRegistry.*;
import static hms.cms.service.CmsServiceRegistry.getProperty;
import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.util.ThrottlingType.MCT;
import static hms.kite.util.ThrottlingType.TPD;
import static java.text.MessageFormat.format;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This will handle all the file download requests
 */
@WebServlet(name = "CmsFileDownloadHandler")
public class CmsFileDownloadHandler extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(CmsFileDownloadHandler.class);

    private static final int DEFAULT_BUFFER_SIZE = 10240;
    private static final String ID = "id";
    private static final String APP_ID = "appid";

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String userAgent = request.getHeader("User-Agent");
        logger.debug("User-Agent is [{}]", userAgent);

        String id = request.getParameter(ID);
        logger.debug("ID is {}", id);

        String appId = request.getParameter(APP_ID);

        if (id == null) {
            sendErrorMessage(format(getProperty("cms.content.not.found.error"), getProperty("cms.system.appstore.name")),
                    response);
            return;
        }

        Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByRequestId(id);

        String fileId;

        Map<String, Object> downloadLink = null;

        if (appId != null) {
            fileId = id;
        } else {
            downloadLink = cmsInternalRepositoryService().findById(id);

            if (downloadLink == null) {
                logger.debug("There is no download link available for the given id [{}]", id);
                sendErrorMessage(format(getProperty("cms.content.not.found.error"), getProperty("cms.system.appstore.name")),
                        response);
                return;
            } else {
                if (!isValidFileDownload(downloadRequest, downloadLink, response)) {
                    resetThrottling(downloadRequest);
                    return;
                }
                fileId = (String) downloadLink.get(buildFileIdK);
            }
        }

        GridFSDBFile file;

        try {
            file = buildFileRepositoryService().getBuildFile(fileId);
        } catch (Exception e) {
            logger.error("Error while reading the file from the database", e);
            sendErrorMessage(format(getProperty("cms.content.not.found.error"), getProperty("cms.system.appstore.name")),
                    response);
            resetThrottling(downloadRequest);
            return;
        }

        if (file == null) {
            logger.debug("There is no file with id {}", fileId);
            sendErrorMessage(format(getProperty("cms.content.not.found.error"), getProperty("cms.system.appstore.name")),
                    response);
            resetThrottling(downloadRequest);
            return;
        }

        String contentType = getServletContext().getMimeType(fileId);

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        String filename = file.getFilename();

        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setHeader("Content-Type", contentType);
        response.setHeader("Content-Length", String.valueOf(file.getLength()));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        boolean isUploadInterrupted = false;

        try {
            input = new BufferedInputStream(file.getInputStream(), DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

            updateDownloadRequestStatus(downloadingK, downloadRequest);

            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            updateDownloadRequestStatus(downloadCompletedK, downloadRequest);

            increaseDownloadCountAndUpdate(successDownloadCountK, downloadCompletedK, downloadLink);

            logger.debug("File uploaded successfully [{}]", filename);
        } catch (IOException e) {
            isUploadInterrupted = true;
            if (("ClientAbortException").equals(e.getClass().getSimpleName())) {
                logger.debug("Client may have aborted the request, ignoring the request and reset throttling");
                resetThrottling(downloadRequest);
            } else {
                handleDownloadError(downloadRequest, downloadLink, e);
            }
        } catch (Exception e) {
            isUploadInterrupted = true;
            handleDownloadError(downloadRequest, downloadLink, e);
        } finally {
            decreaseThrottling(downloadRequest, MCT);

            if (!isUploadInterrupted) {
                close(output);
                close(input);
            }
        }
    }

    private void handleDownloadError(Map<String, Object> downloadRequest, Map<String, Object> downloadLink, Throwable e) {
        logger.error("Error occurred while uploading file", e);

        updateDownloadRequestStatus(downloadFailedK, downloadRequest);

        increaseDownloadCountAndUpdate(failureDownloadCountK, downloadFailedK, downloadLink);
    }

    private void resetThrottling(Map<String, Object> downloadRequest) {
        decreaseThrottling(downloadRequest, MCT);
        decreaseThrottling(downloadRequest, TPD);
    }

    private void decreaseThrottling(Map<String, Object> downloadRequest, ThrottlingType throttlingType) {
        if (downloadRequest != null && downloadRequest.containsKey(applicationIdK)) {
            String throttlingId = getThrottlingId((String) downloadRequest.get(applicationIdK), throttlingType);
            throttlingRepositoryService().increaseThrottling(throttlingId, -1, throttlingType);
        }
    }

    private void updateDownloadRequestStatus(String status, Map<String, Object> downloadRequest) {
        if (downloadRequest != null) {
            downloadRequest.put(statusK, status);
            cmsRequestRepositoryService().updateRequest(downloadRequest);
        }
    }

    private void increaseDownloadCountAndUpdate(String downloadCountKey, String status, Map<String, Object> downloadLink) {
        if (downloadLink != null) {
            int count = 1;

            if (downloadLink.containsKey(downloadCountKey)) {
                count = (Integer) downloadLink.get(downloadCountKey);
                count++;
            }

            downloadLink.put(downloadCountKey, count);
            downloadLink.put(statusK, status);
            cmsInternalRepositoryService().updateDownload(downloadLink);
        }
    }

    private boolean isValidFileDownload(Map<String, Object> downloadRequest,
                                        Map<String, Object> downloadLink,
                                        HttpServletResponse response) throws IOException {
        logger.debug("Validating file download, download link [{}] and download request [{}]", downloadLink, downloadRequest);

        String appId = (String) downloadRequest.get(applicationIdK);

        Map<String, Object> ncsDetailMap = getDownloadableNcs(appId);

        return validateTransactionLimit(appId, ncsDetailMap, MCT, maxConDlK, "cms.mct.exceed.error", response)
                && validateTransactionLimit(appId, ncsDetailMap, TPD, maxDlPerDayK, "cms.tpd.exceed.error", response)
                && validateLinkStatus(appId, downloadLink, response)
                && validateDownloadCount(appId, downloadLink, ncsDetailMap, response);
    }

    private boolean validateTransactionLimit(String appId,
                                             Map<String, Object> ncsDetailMap,
                                             ThrottlingType throttlingType,
                                             String ncsLimitKey,
                                             String errorMsgKey,
                                             HttpServletResponse response) throws IOException {
        String throttlingId = getThrottlingId(appId, throttlingType);
        int latestThrottling = throttlingRepositoryService().increaseThrottling(throttlingId, 1, throttlingType);
        int throttlingLimit = Integer.parseInt((String) ncsDetailMap.get(ncsLimitKey));
        if (throttlingLimit < latestThrottling) {
            logger.debug("Validation failed since the [{}] limit [{}] is exceeded", throttlingType, throttlingLimit);
            sendErrorMessage(format(getProperty(errorMsgKey), getAppName(appId)), response);
            return false;
        }
        return true;
    }

    private boolean validateLinkStatus(String appId, Map<String, Object> downloadLink, HttpServletResponse response) throws IOException {
        Object linkStatus = downloadLink.get(statusK);

        if (initialK.equals(linkStatus)) {
            logger.debug("Ignoring validation since the link status is [{}]", linkStatus);
            return true;
        }

        if (urlExpiredK.equals(linkStatus)) {
            logger.debug("Validation failed since the link status is [{}]", linkStatus);
            sendErrorMessage(format(getProperty("cms.url.expired.error"), getAppName(appId), getProperty("cms.system.appstore.name")),
                    response);
            return false;
        }
        return true;
    }

    private boolean validateDownloadCount(String appId,
                                          Map<String, Object> downloadLink,
                                          Map<String, Object> ncsDetailMap,
                                          HttpServletResponse response) throws IOException {
        int successDownloadCount = getCountFromDownloadLink(downloadLink, successDownloadCountK);
        int failureDownloadCount = getCountFromDownloadLink(downloadLink, failureDownloadCountK);
        int maximumDownloadCount = (Integer) ncsDetailMap.get(dlAtmptK);

        int totalDownloadCount = successDownloadCount + failureDownloadCount + 1;
        if (maximumDownloadCount < totalDownloadCount) {
            logger.debug("Validation failed since the number of download count [{}] exceeded the maximum download count [{}]",
                    totalDownloadCount, maximumDownloadCount);
            sendErrorMessage(format(getProperty("cms.download.retry.count.exceed.error"), getAppName(appId), getProperty("cms.system.appstore.name")),
                    response);
            return false;
        }
        return true;
    }

    private Map<String, Object> getDownloadableNcs(String appId) {
        return ncsRepositoryService().findByAppIdOperatorNcsType(appId, "", downloadableK);
    }

    private String getAppName(String appId) {
        return (String) appRepositoryService().findByAppId(appId).get(nameK);
    }

    private int getCountFromDownloadLink(Map<String, Object> downloadLink, String key) {
        int count = 0;
        if (downloadLink.containsKey(key)) {
            count = (Integer) downloadLink.get(key);
        }
        return count;
    }

    private void sendErrorMessage(String message, HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        out.print(message);
    }

    private void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                logger.error("Error occurred while closing resource ", e);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
