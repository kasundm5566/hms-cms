/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.boot.daemon.impl;

import hms.cms.boot.daemon.CmsDaemon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsInternalRepositoryService;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsDownloadLinkExpirationDaemon implements CmsDaemon {

    private static final Logger logger = LoggerFactory.getLogger(CmsDownloadLinkExpirationDaemon.class);

    @Override
    public void execute() {
        try {
            logger.debug("Start download link expiration cycle");

            Map<String, Object> query = new HashMap<String, Object>();
            query.put(expireDateK, new HashMap<String, Object>() {{
                put("$lt", new Date());
            }});
            query.put(statusK, new HashMap<String, Object>() {{
                put("$ne", urlExpiredK);
            }});
            List<Map<String, Object>> expiredLinksList = cmsInternalRepositoryService().getExpiredLinks(query, 0, 100);

            logger.debug("Number of links to be expired [{}]", expiredLinksList.size());

            for (Map<String, Object> expiredLink : expiredLinksList) {
                Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByRequestId((String) expiredLink.get(idK));
                if(downloadRequest == null) {
                    continue;
                }

                downloadRequest.put(statusK, urlExpiredK);
                cmsRequestRepositoryService().updateRequest(downloadRequest);

                expiredLink.put(statusK, urlExpiredK);
                cmsInternalRepositoryService().updateDownload(expiredLink);

                logger.debug("Expired link [{}] and download request [{}]", expiredLink, downloadRequest);
            }
        } catch (Exception e) {
            logger.error("Error occurred while expiring download links", e);
        }
    }
}
