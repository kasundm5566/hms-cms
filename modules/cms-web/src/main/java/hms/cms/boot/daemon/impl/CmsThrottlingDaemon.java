/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.boot.daemon.impl;

import hms.cms.boot.daemon.CmsDaemon;
import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsThrottlingDaemon implements CmsDaemon {

    private static final Logger logger = LoggerFactory.getLogger(CmsThrottlingDaemon.class);

    private ThrottlingRepositoryService throttlingRepositoryService;
    private ThrottlingType throttlingType;

    @Override
    public void execute() {
        logger.debug("Resetting throttling for [{}]", throttlingType);
        throttlingRepositoryService.resetThrottling(throttlingType);
    }

    public void setThrottlingRepositoryService(ThrottlingRepositoryService throttlingRepositoryService) {
        this.throttlingRepositoryService = throttlingRepositoryService;
    }

    public void setThrottlingType(String type) {
        this.throttlingType = ThrottlingType.valueOf(type);
    }
}
