/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo.impl;

import com.mongodb.*;
import hms.cms.repo.CmsInfindoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.HashMap;
import java.util.Map;

import static com.mongodb.MapReduceCommand.OutputType.REPLACE;
import static hms.cms.common.CmsKeyBox.*;
import static hms.kite.datarepo.common.util.CommonUtil.isCollectionExists;
import static hms.kite.util.KiteKeyBox.cmsDevicesK;
import static hms.kite.util.KiteKeyBox.cmsPlatformsK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsInfindoRepositoryImpl implements CmsInfindoRepository {

    private static final Logger logger = LoggerFactory.getLogger(CmsInfindoRepositoryImpl.class);

    private static final String COLLECTION_NAME = "infindo_downloads";
    private static final String TEMP_1_COLLECTION_NAME = "temp1";
    private static final String TEMP_2_COLLECTION_NAME = "temp2";
    private static final String DEVICES_BRAND_MODEL_MAP_FUNCTION = "function(){emit({brand:this.vendor,model:this.model},{content:[{" + contentIdK + ":this[\"" + cmsContentIdK + "\"],version:\"\"}]})}";
    private static final String DEVICES_BRAND_MAP_FUNCTION = "function(){var brand=this._id.brand;var model=this._id.model;var content=this.value.content;emit(brand,{models:[{model:model,content:content}]})}";
    private static final String REDUCE_CONTENT_FUNCTION = "function(key,values){var content=[];var maxIdIndex=0;var maxValuesIndex=0;var max=0;for(var i=0;i<values.length;i++){for(var j=0;j<values[i].content.length;j++){var current=parseFloat(values[i].content[j]." + contentIdK + ");if(max<current){maxValuesIndex=i;maxIdIndex=j;max=current}}}content.push({" + contentIdK + ":values[maxValuesIndex].content[maxIdIndex]." + contentIdK + ",version:values[maxValuesIndex].content[maxIdIndex].version});return{content:content}}";
    private static final String REDUCE_MODELS_FUNCTION = "function(key,values){var allModels=[];for(var i=0;i<values.length;i++)for(var j=0;j<values[i].models.length;j++)allModels.push({model:values[i].models[j].model,content:values[i].models[j].content});return{models:allModels}}";
    private static final String FINALIZE_FUNCTION = "function(key,value){var models={};for(var i=0;i<value.models.length;i++){models[value.models[i].model]=value.models[i].content}return models}";

    private MongoTemplate mongoTemplate;

    private Map<String, Object> callMapReduce(String firstMapFunction, String firstReduceFunction, String secondMapFunction, String secondReduceFunction, String finalizeFunction, BasicDBObject query) {
        if (!isCollectionExists(mongoTemplate, COLLECTION_NAME)) {
            return new HashMap<String, Object>();
        }

        MapReduceOutput mapReduceOutput = mongoTemplate.getCollection(COLLECTION_NAME).mapReduce(firstMapFunction, firstReduceFunction, TEMP_1_COLLECTION_NAME, query);

        MapReduceCommand mapReduceCommand = new MapReduceCommand(mongoTemplate.getCollection(TEMP_1_COLLECTION_NAME), secondMapFunction, secondReduceFunction, TEMP_2_COLLECTION_NAME, REPLACE, new BasicDBObject());
        mapReduceCommand.setFinalize(finalizeFunction);
        mapReduceOutput = mapReduceOutput.getOutputCollection().mapReduce(mapReduceCommand);

        DBCursor dbCursor = mapReduceOutput.getOutputCollection().find();

        Map<String, Object> resultMap = new HashMap<String, Object>();

        while (dbCursor.hasNext()) {
            DBObject next = dbCursor.next();
            resultMap.put((String) next.get(idK), next.get(valueK));
        }

        mongoTemplate.getCollection(TEMP_1_COLLECTION_NAME).drop();
        mongoTemplate.getCollection(TEMP_2_COLLECTION_NAME).drop();

        return resultMap;
    }

    @Override
    public Map<String, Object> getDownloadSupportInfoByAppId(Map<String, Object> request) {
        Map<String, Object> result = new HashMap<String, Object>();

        BasicDBObject query = new BasicDBObject();
        query.put(appIdK, request.get(applicationIdK));

        Map<String, Object> devicesMap = callMapReduce(DEVICES_BRAND_MODEL_MAP_FUNCTION, REDUCE_CONTENT_FUNCTION, DEVICES_BRAND_MAP_FUNCTION, REDUCE_MODELS_FUNCTION, FINALIZE_FUNCTION, query);

        result.put(cmsDevicesK, devicesMap);
        result.put(cmsPlatformsK, new HashMap<String, Object>());

        return result;
    }

    @Override
    public Map<String, Object> getDownloadDetailsByContentId(String downloadContentId) {
        DBCollection collection = mongoTemplate.getCollection(COLLECTION_NAME);
        BasicDBObject query = new BasicDBObject();
        query.put(cmsContentIdK, downloadContentId);

        Map<String, Object> content = (Map<String, Object>) collection.findOne(query);

        if (content == null) {
            logger.debug("No documents are found for the matching query [{}]", query);
            return null;
        }

        return content;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
