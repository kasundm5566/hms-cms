/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import hms.cms.repo.CmsRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This class implements CmsRequestRepository
 */
public class CmsRequestRepositoryImpl implements CmsRequestRepository {

    private static final Logger logger = LoggerFactory.getLogger(CmsRequestRepositoryImpl.class);

    private static final String COLLECTION_NAME = "request";

    private MongoTemplate mongoTemplate;

    private DBCursor getDbObjects(BasicDBObject query, BasicDBObject sortBy) {
        BasicDBObject keys = new BasicDBObject();
        keys.put(idK, true);
        keys.put(applicationIdK, true);
        keys.put(statusK, true);
        keys.put(requestedDateK, true);
        keys.put(paymentInstructionsK, true);
        keys.put(wapUrlNblK, true);

        return mongoTemplate.getCollection(COLLECTION_NAME).find(query, keys).sort(sortBy);
    }

    @Override
    public Map<String, Object> findByRequestId(String requestId) {
        return (Map<String, Object>) mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(idK, requestId));
    }

    @Override
    public List<Map<String, Object>> findByAddress(String address) {
        BasicDBObject query = new BasicDBObject(addressK, address);
        BasicDBObject sortBy = new BasicDBObject(idK, -1);

        DBCursor dbObjects = getDbObjects(query, sortBy);

        List<Map<String, Object>> downloadRequests = new LinkedList<Map<String, Object>>();

        while (dbObjects.hasNext()) {
            downloadRequests.add((Map<String, Object>) dbObjects.next());
        }

        return downloadRequests;
    }

    @Override
    public Map<String, Object> findByAppIdAndAddress(String appId, String address) {
        BasicDBObject query = new BasicDBObject();
        query.put(applicationIdK, appId);
        query.put(addressK, address);
        BasicDBObject sortBy = new BasicDBObject(idK, -1);

        DBCursor dbObjects = getDbObjects(query, sortBy);

        if (dbObjects.hasNext()) {
            return (Map<String, Object>) dbObjects.next();
        }

        return new HashMap<String, Object>();
    }

    @Override
    public void create(Map<String, Object> request) {
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(request));
    }

    @Override
    public void update(Map<String, Object> request) {
        BasicDBObject query = new BasicDBObject(idK, request.get(idK));
        BasicDBObject updatedObj = new BasicDBObject(request);

        mongoTemplate.getCollection(COLLECTION_NAME).update(query, updatedObj, false, false);
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
