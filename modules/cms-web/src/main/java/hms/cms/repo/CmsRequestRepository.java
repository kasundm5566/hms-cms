/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This class contains all the repository calls related to CmsRequest management
 */
public interface CmsRequestRepository {

    Map<String, Object> findByRequestId(String requestId);

    List<Map<String, Object>> findByAddress(String address);

    Map<String,Object> findByAppIdAndAddress(String appId, String address);

    void create(Map<String, Object> request);

    void update(Map<String, Object> request);
}
