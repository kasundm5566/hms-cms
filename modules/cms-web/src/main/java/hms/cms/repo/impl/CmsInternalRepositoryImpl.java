/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import hms.cms.repo.CmsInternalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.idK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsInternalRepositoryImpl implements CmsInternalRepository {

    private static final Logger logger = LoggerFactory.getLogger(CmsInternalRepositoryImpl.class);

    private static final String COLLECTION_NAME = "internal_downloads";

    private MongoTemplate mongoTemplate;

    @Override
    public Map<String, Object> findById(String id) {
        return (Map<String, Object>) mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(idK, id));
    }

    @Override
    public List<Map<String, Object>> getExpiredLinks(Map<String, Object> query, int skip, int limit) {
        DBCollection downloadList = mongoTemplate.getCollection(COLLECTION_NAME);
        BasicDBObject dbQuery = new BasicDBObject();
        dbQuery.putAll(query);
        DBCursor dbCursor = downloadList.find(dbQuery).skip(skip).limit(limit);
        List<Map<String, Object>> expiredDownloadList = new LinkedList<Map<String, Object>>();
        while (dbCursor.hasNext()) {
            expiredDownloadList.add(dbCursor.next().toMap());
        }
        return expiredDownloadList;
    }

    @Override
    public void createDownload(Map<String, Object> download) {
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(download));
    }

    @Override
    public void updateDownload(Map<String, Object> download) {
        BasicDBObject query = new BasicDBObject(idK, download.get(idK));
        BasicDBObject updatedObj = new BasicDBObject(download);

        mongoTemplate.getCollection(COLLECTION_NAME).update(query, updatedObj, false, false);
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}