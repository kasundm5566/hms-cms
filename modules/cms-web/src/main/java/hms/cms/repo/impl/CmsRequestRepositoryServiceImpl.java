/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo.impl;

import hms.cms.repo.CmsRequestRepository;
import hms.cms.repo.CmsRequestRepositoryService;
import hms.cms.service.CmsTransLog;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This class implements all the service calls
 */
public class CmsRequestRepositoryServiceImpl implements CmsRequestRepositoryService {

    private CmsRequestRepository cmsRequestRepository;

    @Override
    public Map<String, Object> findByRequestId(String requestId) {
        return cmsRequestRepository.findByRequestId(requestId);
    }

    @Override
    public List<Map<String, Object>> findByAddress(String address) {
        return cmsRequestRepository.findByAddress(address);
    }

    @Override
    public Map<String, Object> findByAppIdAndAddress(String appId, String address) {
        return cmsRequestRepository.findByAppIdAndAddress(appId, address);
    }

    @Override
    public void createRequest(Map<String, Object> request) {
        Date date = new Date();

        request.put(createdDateK, date);
        request.put(requestedDateK, date);
        request.put(updatedDateK, date);

        CmsTransLog.log(request);

        cmsRequestRepository.create(request);
    }

    @Override
    public void updateRequest(Map<String, Object> request) {
        Date date = new Date();

        request.put(updatedDateK, date);

        CmsTransLog.log(request);

        cmsRequestRepository.update(request);
    }

    public void setCmsRequestRepository(CmsRequestRepository cmsRequestRepository) {
        this.cmsRequestRepository = cmsRequestRepository;
        }
}
