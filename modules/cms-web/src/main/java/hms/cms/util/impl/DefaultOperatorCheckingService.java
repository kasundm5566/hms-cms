package hms.cms.util.impl;

import com.google.common.base.Optional;
import hms.cms.util.OperatorCheckingService;

/**
 * Default implementation of {@link hms.cms.util.OperatorCheckingService}
 * returns a static operator name
 *
 * @author isuruw@hsenidmobile.com
 */
public class DefaultOperatorCheckingService implements OperatorCheckingService {

    private final String operator;

    public DefaultOperatorCheckingService(String operator) {
        this.operator = operator;
    }

    @Override
    public Optional<String> findOperator(String msisdn) {
        return Optional.of(operator);
    }
}
