package hms.cms.util.impl;

import com.google.common.base.Optional;
import hms.cms.util.OperatorCheckingService;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regex based operator resolving implementation of {@link hms.cms.util.OperatorCheckingService}
 * returns a operator based on configured regex
 *
 * @author isuruw@hsenidmobile.com
 */
public class RegExBasedOperatorCheckingService implements OperatorCheckingService {

    private final Map<String, String> regexMap;
    private final Map<Pattern, String> patternMap;

    public RegExBasedOperatorCheckingService(Map<String, String> regexMap) {
        this.regexMap = regexMap;
        this.patternMap = new HashMap<>();
        for (String regex : regexMap.keySet()) {
            Pattern pattern = Pattern.compile(regex);
            patternMap.put(pattern, regexMap.get(regex));
        }
    }


    @Override
    public Optional<String> findOperator(String msisdn) {
        for (Pattern pattern : patternMap.keySet()) {
            Matcher matcher = pattern.matcher(msisdn);
            if(matcher.matches()) {
                return Optional.of(patternMap.get(pattern));
            }
        }
        return Optional.absent();
    }
}
