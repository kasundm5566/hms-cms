package hms.cms.util;

import com.google.common.base.Optional;

/**
 * @author isuruw@hsenidmobile.com
 */
public interface OperatorCheckingService {

    /**
     * returns the operator of particular mobile number
     *
     * @param msisdn the msisdn
     * @return the string
     */
    Optional<String> findOperator(String msisdn);
}
