/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.infindo;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class InfindoApp {

    private String id;
    private String name;
    //private String version;
    private String category;
    //private String subCategory;
    private String description;
    //private String fileSize;
    private String supportedDevices;
    //private String iconSmall;
    private String iconBig;
    private String screen1;
    private String screen2;

    private Map<String, Map<String, String>> supportedDeviceMap;

    public InfindoApp() {
        supportedDeviceMap = new HashMap<String, Map<String, String>>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String appName) {
        this.name = appName.replaceAll("[^a-zA-Z0-9]", " ").trim();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSupportedDevices() {
        return supportedDevices;
    }

    public void setSupportedDevices(String supportedDevices) {
        this.supportedDevices = supportedDevices;
    }

    public String getIconBig() {
        return iconBig;
    }

    public void setIconBig(String iconBig) {
        this.iconBig = iconBig;
    }

    public String getScreen1() {
        return screen1;
    }

    public void setScreen1(String screen1) {
        this.screen1 = screen1;
    }

    public String getScreen2() {
        return screen2;
    }

    public void setScreen2(String screen2) {
        this.screen2 = screen2;
    }

    public Map<String, Map<String, String>> getSupportedDeviceMap() {
        return supportedDeviceMap;
    }

    public void setSupportedDeviceMap(Map<String, Map<String, String>> supportedDeviceMap) {
        this.supportedDeviceMap = supportedDeviceMap;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n =============================================================")
                .append("\n Application ID: ").append(id)
                .append("\n  Application Name: ").append(name)
                .append("\n  Category: ").append(category)
                .append("\n  Description: ").append(description)
                .append("\n  Supported Devices: ").append(supportedDevices)
                .append("\n  Icon Big: ").append(iconBig)
                .append("\n  Screen 1: ").append(screen1)
                .append("\n  Screen 2: ").append(screen2)
                .append("\n  Supported Device Map: ").append(supportedDeviceMap)
                .append("\n =============================================================").toString();
    }
}
