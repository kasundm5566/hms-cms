/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.common;

import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.*;

import static hms.kite.util.KiteKeyBox.*;
import static java.lang.Integer.parseInt;
import static java.util.Calendar.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class Utils {

    private static Logger logger = LoggerFactory.getLogger(Utils.class);
    private static ResourceBundle resourceBundle;

    public static final String RESOURCE_NAME = "cms";

    static {
        Utils.resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME, new Locale("US"));
    }

    public static String getMsg(String key, String... arguments) {
        try {
            if (arguments != null) {
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            } else {
                return resourceBundle.getString(key);
            }
        } catch (MissingResourceException mre) {
            logger.error("Message key not found [" + key + "]");
            return '!' + key;
        }
    }

    public static Date getExpireDate(Date currentDate, String timeType, String time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);

        if (minutesK.equals(timeType)) {
            calendar.set(MINUTE, calendar.get(MINUTE) + parseInt(time));
        } else if (hoursK.equals(timeType)) {
            calendar.set(HOUR_OF_DAY, calendar.get(HOUR_OF_DAY) + parseInt(time));
        } else if (daysK.equals(timeType)) {
            calendar.set(DAY_OF_YEAR, calendar.get(DAY_OF_YEAR) + parseInt(time));
        }

        return calendar.getTime();
    }

    public static String getThrottlingId(String appId, ThrottlingType throttlingType) {
        StringBuilder sb = new StringBuilder();
        return sb.append(appId).append(".").
                append(downloadableK).append(".").
                append(throttlingType.getValue()).toString();
    }
}
