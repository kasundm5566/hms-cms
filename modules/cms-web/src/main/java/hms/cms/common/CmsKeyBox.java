/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.common;

import hms.kite.util.KiteKeyBox;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsKeyBox extends KiteKeyBox {

    /**
     * SDP NBL Api Keys
     * */
    public static final String accountIdK                   = "accountId";
    public static final String applicationIdK               = "applicationId";
    public static final String applicationIdListK           = "applicationIds";
    public static final String balanceDueK                  = "balanceDue";
    public static final String contentIdK                   = "contentId";
    public static final String contentK                     = "content";
    public static final String currencyK                    = "currency";
    public static final String deliveryStatusRequestK       = "deliveryStatusRequest";
    public static final String destinationAddressesK        = "destinationAddresses";
    public static final String downloadRequestIdK           = "downloadRequestId";
    public static final String externalTrxIdK               = "externalTrxId";
    public static final String internalTrxIdK               = "internalTrxId";
    public static final String longDescriptionK             = "longDescription";
    public static final String messageK                     = "message";
    public static final String resultK                      = "result";
    public static final String referenceIdK                 = "referenceId";
    public static final String requestedDateK               = "requestedDate";
    public static final String paidAmountK                  = "paidAmount";
    public static final String passwordK                    = "password";
    public static final String paymentInstrumentListK       = "paymentInstrumentList";
    public static final String paymentInstrumentNameK       = "paymentInstrumentName";
    public static final String paymentInstructionsK         = "paymentInstructions";
    public static final String sourceAddressNblK            = "sourceAddress";
    public static final String subscriberIdK                = "subscriberId";
    public static final String statusRequestK               = "statusRequest";
    public static final String statusCodeK                  = "statusCode";
    public static final String statusDetailK                = "statusDescription";
    public static final String shortDescriptionK            = "shortDescription";
    public static final String timestampK                   = "timestamp";
    public static final String totalAmountK                 = "totalAmount";
    public static final String versionK                     = "version";
    public static final String wapPushTypeNblK		        = "wapPushType";
    public static final String wapUrlNblK			        = "wapUrl";
    public static final String chargingStatusCodeK          = "charging-status-code";
    public static final String chargingStatusDescriptionsK = "charging-status-description";
    public static final String downloadRequestAdditionalParamsK = "additionalParams";
    /**
     * SDP Success Codes
     * */
    public static final String successCode                      = "S1000";

    /**
     * SDP Partial Success Codes
     * */
    public static final String paymentPendingPartialSuccessCode = "P1003";
    public static final String paymentSuccessPartialSuccessCode = "S1000";

    /**
     * SDP Error Codes
     * */
    public static final String internalErrorErrorCode           = "E1600";

    /**
     * CMS Keys
     * */
    public static final String chargeRequestK                    = "charge";
    public static final String chargingNotificationRequestK      = "charge-notification";
    public static final String controlRequestK                   = "control";
    public static final String cmsChargingAmountK                = "charging-" + amountK;
    public static final String cmsChargingTypeK                  = "charging-" + typeK;
    public static final String cmsContentIdK                     = "content-id";
    public static final String downloadRequestK                  = "download";
    public static final String failureDownloadCountK             = "failure-download-count";
    public static final String infindoDownloadK                  = "infindo";
    public static final String internalDownloadK	             = "internal";
    public static final String queryRequestK                     = "query";
    public static final String queryTypeK                        = "query-type";
    public static final String senderAddressK                    = "sender-address";
    public static final String successDownloadCountK             = "success-download-count";
    public static final String channelK                          = "channel";


    /**
     * CMS Request Statuses
     * */
    public static final String acceptedK                = "accepted";
    public static final String downloadingK             = "downloading";
    public static final String downloadCompletedK       = "download-completed";
    public static final String downloadFailedK          = "download-failed";
    public static final String eligibleK                = "eligible";
    public static final String chargingFailedK          = "charging-failed";
    public static final String chargingPendingK         = "charging-pending";
    public static final String chargingRequestedK       = "charging-requested";
    public static final String wapPushDeliveredK        = "wap-push-delivered";
    public static final String wapPushFailedK           = "wap-push-failed";
    public static final String urlExpiredK              = "url-expired";

    /**
     * CMS Success Codes
     * */
    public static final String cmsSuccessCode                        = "S1000";

    /**
     * CMS Partial Success Codes
     * */
    public static final String cmsPartialSuccessCode                 = "P1500";
    public static final String cmsWithPiListPartialSuccessCode       = "P1501";
    public static final String cmsWithInstructionPartialSuccessCode  = "P1502";

    /**
     * CMS Error Codes
     * */
    public static final String cmsAppIdErrorCode                     = "E1820";
    public static final String cmsAddressErrorCode                   = "E1838";
    public static final String cmsContentIdErrorCode                 = "E1828";
    public static final String cmsContentNotFoundErrorCode           = "E1829";
    public static final String cmsChargingFailedErrorCode            = "E1900";
    public static final String cmsChargingUnknownTypeErrorCode       = "E1901";
    public static final String cmsDownloadRequestIdErrorCode         = "E1831";
    public static final String cmsExternalTransactionIdErrorCode     = "E1834";
    public static final String cmsPaymentInstrumentNameErrorCode     = "E1832";
    public static final String cmsInvalidRequestErrorCode            = "E1830";
    public static final String cmsNoResultsErrorCode                 = "E1821";
    public static final String cmsRecipientAddressErrorCode          = "E1825";
    public static final String cmsReferenceIdErrorCode               = "E1836";
    public static final String cmsChargingRequestedErrorCode         = "E1837";
    public static final String cmsDownloadRequestNotFoundErrorCode   = "E1833";
    public static final String cmsSourceAddressErrorCode             = "E1824";
    public static final String cmsStatusCodeErrorCode                = "E1835";
    public static final String cmsSystemErrorErrorCode               = "E1850";
    public static final String cmsWapPushMessageErrorCode            = "E1823";
    public static final String cmsWapPushTypeErrorCode               = "E1826";
    public static final String cmsWapUrlErrorCode                    = "E1827";

    /**
     * CMS Events
     * */
    public static final String RECEIVED_DOWNLOAD_REQUEST            = "received-download-request";
    public static final String RECEIVED_CHARGE_REQUEST              = "received-charge-request";
    public static final String SAVE_DOWNLOAD_REQUEST                = "save-download-request";
    public static final String UPDATE_DOWNLOAD_REQUEST              = "update-download-request";

    public static final String mpaisaPaymentRefId = "payment-ref-id";
}
