/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.common;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public final class Http {

    private static WebClient webClient(String nblUrlK, String header, String accept) {
        List<Object> bodyProviders = new ArrayList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create(nblUrlK, bodyProviders);
        webClient.header("Content-Type", header);
        webClient.accept(accept);
        return webClient;
    }

    public static WebClient createWebClient(String nblUrlK) {
        return webClient(nblUrlK, "application/json", "application/json");
    }

    public static WebClient createWebClientPlainText(String url) {
        return webClient(url, "text/plain", "text/plain");
    }

    public static WebClient getWebClientJpegClient(String imageUrl) {
        return webClient(imageUrl, "image/jpeg", "image/jpeg");
    }

    public static String get(String url) throws IOException {
        return Http.inputStreamToString(Http.getInputStream(url));
    }

    public static InputStream getInputStream(String url) {
        InputStream response;
        for (; ; ) {
            WebClient webClient = Http.createWebClientPlainText(url);
            try {
                webClient.get();
            } catch (Exception ex) {
                continue;
            }
            Response responseObject = webClient.getResponse();

            int status = responseObject.getStatus();
            if (status == 503) {
                continue;
            } else if (status == 200) {
                response = (InputStream) responseObject.getEntity();
                break;
            } else {
                throw new IllegalStateException("unknown status code " + status);
            }
        }
        return response;
    }

    public static String inputStreamToString(InputStream inputStream) throws IOException {
        return IOUtils.toString(inputStream, "UTF-8");
    }

}
