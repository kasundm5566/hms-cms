<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:mongo="http://www.springframework.org/schema/data/mongo"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
            http://www.springframework.org/schema/util
            http://www.springframework.org/schema/util/spring-util.xsd
            http://www.springframework.org/schema/data/mongo
            http://www.springframework.org/schema/data/mongo/spring-mongo.xsd
            http://www.springframework.org/schema/context
            http://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath:cms.properties"/>

    <import resource="classpath:sdp-repo-spring.xml"/>
    <import resource="classpath:cms-operator-beans.xml"/>

    <context:annotation-config/>

    <context:component-scan base-package="hms.cms"/>

    <mongo:mongo id="cms.mongo" host="${cms.db.host}" port="${cms.db.port}">
        <mongo:options connections-per-host="50"
                       threads-allowed-to-block-for-connection-multiplier="5"
                       connect-timeout="1000"
                       max-wait-time="1500"
                       auto-connect-retry="true"
                       socket-keep-alive="true"
                       socket-timeout="1500"
                       slave-ok="false"
                       write-number="0"
                       write-timeout="0"
                       write-fsync="false"/>
    </mongo:mongo>

    <bean id="cms.mongo.template" class="org.springframework.data.mongodb.core.MongoTemplate">
        <constructor-arg ref="cms.mongo"/>
        <constructor-arg value="${cms.db.name}"/>
    </bean>

    <bean id="hms.kite.util.sysUtil" class="hms.kite.util.SystemUtil" p:systemId="${cms.system.id}"/>

    <bean id="cms.service.cmsSdpService" class="hms.cms.service.impl.CmsSdpServiceImpl"/>

    <bean id="cms.handler.cmsDownloadRequestHandler" class="hms.cms.handlers.download.CmsDownloadRequestHandler">
        <property name="operatorCheckingService" ref="operatorCheckingService"/>
    </bean>

    <bean id="cms.handler.cmsDownloadChargeRequestHandler" class="hms.cms.handlers.charge.CmsDownloadChargeRequestHandler"/>

    <bean id="cms.handler.cmsChargingNotificationRequestHandler"
          class="hms.cms.handlers.chargingnotification.CmsChargingNotificationRequestHandler"/>

    <bean id="cms.handler.cmsQueryRequestHandler" class="hms.cms.handlers.query.CmsQueryRequestHandler"/>

    <bean id="cms.handler.cmsControlRequestHandler" class="hms.cms.handlers.control.CmsControlRequestHandler"/>

    <bean id="cms.service.cmsService" class="hms.cms.service.impl.CmsServiceImpl"
          p:cmsDownloadRequestHandler-ref="cms.handler.cmsDownloadRequestHandler"
          p:cmsDownloadChargeRequestHandler-ref="cms.handler.cmsDownloadChargeRequestHandler"
          p:cmsChargingNotificationRequestHandler-ref="cms.handler.cmsChargingNotificationRequestHandler"
          p:cmsQueryRequestHandler-ref="cms.handler.cmsQueryRequestHandler"
          p:cmsControlRequestHandler-ref="cms.handler.cmsControlRequestHandler"/>

    <bean id="cms.service.CmsEventLog" class="hms.cms.service.impl.CmsEventLogImpl"/>

    <bean id="cms.repo.cmsRequestRepository" class="hms.cms.repo.impl.CmsRequestRepositoryImpl"
          p:mongoTemplate-ref="cms.mongo.template"/>

    <bean id="cms.repo.cmsInfindoRepository" class="hms.cms.repo.impl.CmsInfindoRepositoryImpl"
          p:mongoTemplate-ref="cms.mongo.template"/>

    <bean id="cms.repo.cmsInternalRepository" class="hms.cms.repo.impl.CmsInternalRepositoryImpl"
          p:mongoTemplate-ref="cms.mongo.template"/>

    <bean id="cms.repo.buildFileMongoRepository" class="hms.kite.datarepo.mongodb.BuildFileMongoRepository"
          p:mongoTemplate-ref="cms.mongo.template"/>

    <bean id="cms.repo.throttlingMongoDbRepository" class="hms.kite.datarepo.mongodb.ThrottlingMongoDbRepository"
          p:mongoTemplate-ref="cms.mongo.template"/>

    <bean id="cms.repo.service.cmsRequestRepositoryService" class="hms.cms.repo.impl.CmsRequestRepositoryServiceImpl"
          p:cmsRequestRepository-ref="cms.repo.cmsRequestRepository"/>

    <bean id="cms.repo.service.cmsInfindoRepositoryService"
          class="hms.cms.repo.impl.CmsInfindoRepositoryServiceImpl"
          p:cmsInfindoRepository-ref="cms.repo.cmsInfindoRepository"/>

    <bean id="cms.repo.service.cmsInternalRepositoryService" class="hms.cms.repo.impl.CmsInternalRepositoryServiceImpl"
          p:cmsInternalRepository-ref="cms.repo.cmsInternalRepository"/>

    <bean id="cms.repo.service.buildFileRepositoryService" class="hms.kite.datarepo.impl.BuildFileRepositoryServiceImpl"
          p:buildFileMongoRepository-ref="cms.repo.buildFileMongoRepository"/>

    <bean id="cms.repo.service.throttlingRepositoryService"
          class="hms.kite.datarepo.impl.ThrottlingRepositoryServiceImpl"
          p:throttlingRepository-ref="cms.repo.throttlingMongoDbRepository"/>

    <bean id="downloadLinkExpirationDaemon" class="hms.cms.boot.daemon.impl.CmsDownloadLinkExpirationDaemon"/>

    <bean id="downloadLinkExpirationScheduler"
          class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="downloadLinkExpirationDaemon"/>
        <property name="targetMethod" value="execute"/>
    </bean>

    <bean id="downloadLinkExpirationTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="downloadLinkExpirationScheduler"/>
        <property name="startDelay" value="${cms.link.expiration.scheduler.daemon.cron.trigger.bean.start.delay.time}"/>
        <property name="cronExpression" value="${cms.link.expiration.scheduler.daemon.cron.expression}"/>
    </bean>

    <bean id="throttlingDaemon" class="hms.cms.boot.daemon.impl.CmsThrottlingDaemon"
          p:throttlingRepositoryService-ref="cms.repo.service.throttlingRepositoryService"
          p:throttlingType="TPD"/>

    <bean id="tpdThrottlingScheduler" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="throttlingDaemon"/>
        <property name="targetMethod" value="execute"/>
    </bean>

    <bean id="tpdThrottlingTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="tpdThrottlingScheduler"/>
        <property name="startDelay" value="${cms.throttling.daemon.cron.trigger.bean.start.delay.time}"/>
        <property name="cronExpression" value="${cms.throttling.daemon.cron.expression}"/>
    </bean>

    <bean id="schedulerFactoryBean" class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="triggers">
            <util:list>
                <ref bean="downloadLinkExpirationTrigger"/>
                <ref bean="tpdThrottlingTrigger"/>
            </util:list>
        </property>
    </bean>

    <bean id="cms.staticDependencyInjector" class="hms.commons.StaticDependencyInjector" init-method="doInitialize"
          lazy-init="false">
        <property name="dependencySet">
            <util:map>
                <entry key="cmsProperties">
                    <util:properties location="classpath:cms.properties"/>
                </entry>
                <entry key="cmsSdpService" value-ref="cms.service.cmsSdpService"/>
                <entry key="cmsService" value-ref="cms.service.cmsService"/>
                <entry key="cmsEventLog" value-ref="cms.service.CmsEventLog"/>
                <entry key="downloadUrlHandlers" value-ref="cms.service.downloadUrlHandlerMap"/>
                <entry key="cmsRequestRepositoryService"
                       value-ref="cms.repo.service.cmsRequestRepositoryService"/>
                <entry key="cmsInfindoRepositoryService"
                       value-ref="cms.repo.service.cmsInfindoRepositoryService"/>
                <entry key="cmsInternalRepositoryService" value-ref="cms.repo.service.cmsInternalRepositoryService"/>
                <entry key="buildFileRepositoryService" value-ref="cms.repo.service.buildFileRepositoryService"/>
                <entry key="throttlingRepositoryService"
                       value-ref="cms.repo.service.throttlingRepositoryService"/>
            </util:map>
        </property>
        <property name="staticClassSet">
            <util:set>
                <value>hms.cms.service.CmsServiceRegistry</value>
                <value>hms.cms.repo.CmsRepositoryServiceRegistry</value>
            </util:set>
        </property>
    </bean>
</beans>
