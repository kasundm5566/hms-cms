package hms.cms.service;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hms.kite.util.KiteKeyBox.telK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Test
public class CmsTransLogTest {
    @Test
    public void testTransformToFlatMap() throws Exception {
        Map<String, Object> map1 = new HashMap<String, Object>();
        Map<String, Object> map11 = new HashMap<String, Object>();
        Map<String, Object> map111 = new HashMap<String, Object>();


        map1.put("sub-map", map11);
        map1.put("dummy-string-1", "dummy1");
        map11.put("sub-map-level-2", map111);
        map11.put("dummy-string-2", "dummy2");
        map111.put("dummy-string-3", "dummy3");

        Map<String, Object> flatMap = CmsTransLog.transformToFlatMap(map1, "");
        assertEquals(flatMap.get("sub-map.sub-map-level-2.dummy-string-3"), "dummy3");
    }

    @Test
    public void testMsisdnRegEx() {
        Pattern pattern = Pattern.compile("^" + telK + ":(?<msisdn>[\\d]+)" + "$");
        Matcher matcher = pattern.matcher("tel:6799935431");
        assertTrue(matcher.matches());
        assertEquals(matcher.group("msisdn"), "6799935431");
    }
}
