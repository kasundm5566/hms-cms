package hms.cms.util.impl;

import com.google.common.collect.Maps;
import hms.cms.util.OperatorCheckingService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.*;

public class RegExBasedOperatorCheckingServiceTest {

    private OperatorCheckingService operatorCheckingService;

    @BeforeMethod
    public void setUp() throws Exception {
        HashMap<String, String> regExMap = Maps.newHashMap();
        regExMap.put("0703([0-9]{7})", "mtn");
        regExMap.put("0706([0-9]{7})", "mtn");
        regExMap.put("9476([0-9]{7})", "dialog");

        operatorCheckingService = new RegExBasedOperatorCheckingService(regExMap);
    }

    @Test
    public void testFindOperator() throws Exception {
        assertEquals(operatorCheckingService.findOperator("07061234567").get(), "mtn");
        assertEquals(operatorCheckingService.findOperator("94766789090").get(), "dialog");
    }
}