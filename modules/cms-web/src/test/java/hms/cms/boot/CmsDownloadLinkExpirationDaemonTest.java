/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.boot;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import hms.cms.boot.daemon.impl.CmsDownloadLinkExpirationDaemon;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static org.testng.Assert.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:cms-test-beans.xml"})
public class CmsDownloadLinkExpirationDaemonTest extends AbstractTestNGSpringContextTests {

    private static final String INTERNAL_DOWNLOADS_COLLECTION_NAME = "internal_downloads";
    private static final String REQUEST_COLLECTION_NAME = "request";

    @Resource
    @Qualifier("cms.mongo.template")
    private MongoTemplate template;

    private DBCollection request;
    private DBCollection internalDownloads;

    @BeforeMethod
    public void setUp() throws Exception {
        template.getDb().dropDatabase();

        request = template.getCollection(REQUEST_COLLECTION_NAME);
        internalDownloads = template.getCollection(INTERNAL_DOWNLOADS_COLLECTION_NAME);

        Map<String, Object> request1 = new HashMap<String, Object>();
        request1.put(idK, "1312042514090012");
        request1.put(applicationIdK, "APP_000000");
        request1.put(cmsRequestTypeK, "download");
        request1.put(cmsChargingTypeK, "free");
        request1.put(cmsRepositoryK, "internal");
        request1.put(statusK, wapPushDeliveredK);
        request1.put(wapUrlNblK, "");
        request1.put(addressK, "tel:254707894561");

        Map<String, Object> contentInfoMap1 = new HashMap<String, Object>();
        contentInfoMap1.put(idK, "1312042514090012");
        contentInfoMap1.put(appIdK, "APP_000000");
        contentInfoMap1.put(buildFileIdK, "112042514090012");

        request1.put(cmsContentInfoK, contentInfoMap1);

        Map<String, Object> request2 = new HashMap<String, Object>();
        request2.put(idK, "1312042514090013");
        request2.put(applicationIdK, "APP_000000");
        request2.put(cmsRequestTypeK, "download");
        request2.put(cmsChargingTypeK, "free");
        request2.put(cmsRepositoryK, "internal");
        request2.put(statusK, chargingPendingK);
        request2.put(addressK, "tel:254707894561");
        request2.put(paymentInstructionsK, "instructions");

        Map<String, Object> contentInfoMap2 = new HashMap<String, Object>();
        contentInfoMap2.put(idK, "1312042514090013");
        contentInfoMap2.put(appIdK, "APP_000000");
        contentInfoMap2.put(buildFileIdK, "1312042514090013");

        request2.put(cmsContentInfoK, contentInfoMap2);

        Map<String, Object> request3 = new HashMap<String, Object>();
        request3.put(idK, "1312042514090014");
        request3.put(applicationIdK, "APP_000000");
        request3.put(cmsRequestTypeK, "download");
        request3.put(cmsChargingTypeK, "free");
        request3.put(cmsRepositoryK, "internal");
        request3.put(statusK, chargingPendingK);
        request3.put(addressK, "tel:254707894563");

        Map<String, Object> contentInfoMap3 = new HashMap<String, Object>();
        contentInfoMap3.put(idK, "1312042514090014");
        contentInfoMap3.put(appIdK, "APP_000000");
        contentInfoMap3.put(buildFileIdK, "1312042514090014");

        request3.put(cmsContentInfoK, contentInfoMap3);

        request.save(new BasicDBObject(request1));
        request.save(new BasicDBObject(request2));
        request.save(new BasicDBObject(request3));

        Map<String, Object> download1 = new HashMap<String, Object>();
        download1.put(idK, "1312042514090012");
        download1.put(appIdK, "APP_000000");
        download1.put(buildFileIdK, "222222222222222");
        Calendar calendar = Calendar.getInstance();
        Date currentDate = new Date();
        calendar.setTime(currentDate);
        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - 1);
        download1.put(expireDateK, calendar.getTime());

        Map<String, Object> download2 = new HashMap<String, Object>();
        download2.put(idK, "1312042514090013");
        download2.put(appIdK, "APP_000000");
        download2.put(buildFileIdK, "222222222222222");
        calendar.setTime(currentDate);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
        download2.put(expireDateK, calendar.getTime());

        Map<String, Object> download3 = new HashMap<String, Object>();
        download3.put(idK, "1312042514090014");
        download3.put(appIdK, "APP_000000");
        download3.put(buildFileIdK, "222222222222222");
        calendar.setTime(currentDate);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 1);
        download3.put(expireDateK, calendar.getTime());

        internalDownloads.save(new BasicDBObject(download1));
        internalDownloads.save(new BasicDBObject(download2));
        internalDownloads.save(new BasicDBObject(download3));
    }

    @AfterMethod
    public void tearDown() throws Exception {
        template.getDb().dropDatabase();
    }

    @Test
    public void testExecute() throws Exception {
        DBObject request1 = request.findOne(new BasicDBObject(idK, "1312042514090012"));
        DBObject download1 = internalDownloads.findOne(new BasicDBObject(idK, "1312042514090012"));

        assertTrue(request1.containsField(statusK));
        assertEquals(wapPushDeliveredK, request1.get(statusK));
        assertFalse(download1.containsField(statusK));

        CmsDownloadLinkExpirationDaemon expirationDaemon = new CmsDownloadLinkExpirationDaemon();
        expirationDaemon.execute();

        request1 = request.findOne(new BasicDBObject(idK, "1312042514090012"));
        download1 = internalDownloads.findOne(new BasicDBObject(idK, "1312042514090012"));

        assertEquals(request1.get(statusK), urlExpiredK);
        assertEquals(request1.get(statusK), download1.get(statusK));
    }
}
