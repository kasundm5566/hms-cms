/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo;

import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsInfindoRepositoryService;
import static hms.kite.util.KiteKeyBox.cmsDevicesK;
import static hms.kite.util.KiteKeyBox.cmsPlatformsK;
import static junit.framework.Assert.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:cms-test-beans.xml"})
public class CmsInfindoRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource
    @Qualifier("cms.mongo.template")
    MongoTemplate template;

    private final String COLLECTION_NAME = "infindo_downloads";

    private Map<String, Object> makeContent(String contentId) {
        BasicDBObject content = new BasicDBObject();
        content.put(contentIdK, contentId);
        content.put(versionK, "");

        return content;
    }

    @BeforeClass
    public void cleanCollection() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @AfterMethod
    public void tearDown() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @Test
    public void testGetDownloadSupportInfoByAppId() {
        Map<String, Object> download1 = new HashMap<String, Object>();
        download1.put(idK, "101111232020460001");
        download1.put(cmsContentIdK, "101111232020460001");
        download1.put(appIdK, "APP_000001");
        download1.put("vendor", "Huawei");
        download1.put("model", "U8230");

        Map<String, Object> download2 = new HashMap<String, Object>();
        download2.put(idK, "101111232020460002");
        download2.put(cmsContentIdK, "101111232020460002");
        download2.put(appIdK, "APP_000001");
        download2.put("vendor", "Motorola");
        download2.put("model", "XT810");

        Map<String, Object> download3 = new HashMap<String, Object>();
        download3.put(idK, "101111232020460003");
        download3.put(cmsContentIdK, "101111232020460003");
        download3.put(appIdK, "APP_000001");
        download3.put("vendor", "Motorola");
        download3.put("model", "DEFY");

        Map<String, Object> download4 = new HashMap<String, Object>();
        download4.put(idK, "101111232020460004");
        download4.put(cmsContentIdK, "101111232020460004");
        download4.put(appIdK, "APP_000001");
        download4.put("vendor", "Motorola");
        download4.put("model", "BACKFLIP");

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download1));
        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download2));
        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download3));
        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download4));

        Map<String, Object> query = new HashMap<String, Object>();
        query.put(applicationIdK, "APP_000001");

        Map<String, Object> contentMap = cmsInfindoRepositoryService().getDownloadSupportInfoByAppId(query);

        Map<String, Object> devices = new HashMap<String, Object>();
        BasicDBObject models1 = new BasicDBObject();
        models1.put("U8230", Arrays.asList(makeContent("101111232020460001")));
        devices.put("Huawei", models1);

        BasicDBObject models2 = new BasicDBObject();
        models2.put("BACKFLIP", Arrays.asList(makeContent("101111232020460004")));
        models2.put("XT810", Arrays.asList(makeContent("101111232020460002")));
        models2.put("DEFY", Arrays.asList(makeContent("101111232020460003")));
        devices.put("Motorola", models2);

        final Map<String, Object> appDevicesAndPlatforms = new HashMap<String, Object>();
        appDevicesAndPlatforms.put(cmsDevicesK, devices);
        appDevicesAndPlatforms.put(cmsPlatformsK, new HashMap<String, Object>());

        System.out.println("Expected : {" + appDevicesAndPlatforms + "} \n" +
                "Actual   : {" + contentMap + "}");

        assertNotNull(contentMap);
        assertEquals(appDevicesAndPlatforms, contentMap);
    }

    @Test
    public void testGetDownloadDetailsByContentId() {
        Map<String, Object> dbObject = new HashMap<String, Object>();
        dbObject.put(idK, "101111232020460001");
        dbObject.put(cmsContentIdK, "101111232020460001");
        dbObject.put(appIdK, "APP_000001");
        dbObject.put("vendor", "Huawei");
        dbObject.put("model", "U8230");

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(dbObject));

        Map<String, Object> contentMap = cmsInfindoRepositoryService().getDownloadDetailsByContentId("101111232020460001");

        assertNotNull(contentMap);
        assertEquals(dbObject, contentMap);
    }
}
