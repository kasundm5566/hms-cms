/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo;

import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.*;

import static hms.cms.common.CmsKeyBox.idK;
import static hms.cms.common.CmsKeyBox.urlExpiredK;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsInternalRepositoryService;
import static hms.kite.util.KiteKeyBox.*;
import static junit.framework.Assert.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:cms-test-beans.xml"})
public class CmsInternalRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource
    @Qualifier("cms.mongo.template")
    MongoTemplate template;

    private final String COLLECTION_NAME = "internal_downloads";

    @BeforeClass
    public void cleanCollection() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @AfterMethod
    public void tearDown() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @Test
    public void testCreateDownload() {
        Map<String, Object> download = new HashMap<String, Object>();
        download.put(idK, "111111111");
        download.put(appIdK, "APP_000000");
        download.put(buildFileIdK, "222222222222222");

        cmsInternalRepositoryService().createDownload(download);

        Map<String, Object> dbObject = cmsInternalRepositoryService().findById("111111111");

        assertNotNull(dbObject);
        assertEquals(download, dbObject);
    }

    @Test
    public void testUpdateDownload() {
        Map<String, Object> download = new HashMap<String, Object>();
        download.put(idK, "111111111");
        download.put(appIdK, "APP_000000");
        download.put(buildFileIdK, "222222222222222");

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download));

        download.put(statusK, urlExpiredK);
        download.put(expireDateK, new Date());

        cmsInternalRepositoryService().updateDownload(download);

        Map<String, Object> dbObject = cmsInternalRepositoryService().findById("111111111");

        assertNotNull(dbObject);
        assertEquals(download, dbObject);
    }

    @Test
    public void testFindById() {
        Map<String, Object> download1 = new HashMap<String, Object>();
        download1.put(idK, "111111111");
        download1.put(appIdK, "APP_000000");
        download1.put(buildFileIdK, "222222222222222");

        Map<String, Object> download2 = new HashMap<String, Object>();
        download2.put(idK, "222222222");
        download2.put(appIdK, "APP_000000");
        download2.put(buildFileIdK, "222222222222222");

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download1));

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download2));

        Map<String, Object> dbObject1 = cmsInternalRepositoryService().findById("111111111");

        assertNotNull(dbObject1);
        assertEquals(download1, dbObject1);

        Map<String, Object> dbObject2 = cmsInternalRepositoryService().findById("222222222");

        assertNotNull(dbObject2);
        assertEquals(download2, dbObject2);
    }

    @Test
    public void testGetExpiredLinks() {
        Map<String, Object> download1 = new HashMap<String, Object>();
        download1.put(idK, "111111111");
        download1.put(appIdK, "APP_000000");
        download1.put(buildFileIdK, "222222222222222");
        Calendar calendar = new GregorianCalendar();
        calendar.set(2012, Calendar.SEPTEMBER, 8, 0, 0);
        download1.put(expireDateK, calendar.getTime());

        Map<String, Object> download2 = new HashMap<String, Object>();
        download2.put(idK, "222222222");
        download2.put(appIdK, "APP_000000");
        download2.put(buildFileIdK, "222222222222222");

        Map<String, Object> download3 = new HashMap<String, Object>();
        download3.put(idK, "333333333");
        download3.put(appIdK, "APP_000000");
        download3.put(buildFileIdK, "222222222222222");
        calendar.set(2012, Calendar.SEPTEMBER, 8, 0, 0);
        download3.put(expireDateK, calendar.getTime());
        download3.put(statusK, urlExpiredK);

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download1));

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download2));

        template.getCollection(COLLECTION_NAME).save(new BasicDBObject(download3));

        Map<String, Object> query = new HashMap<String, Object>();
        query.put(expireDateK, new HashMap<String, Object>() {{
            put("$lt", new Date());
        }});
        query.put(statusK, new HashMap<String, Object>() {{
            put("$ne", urlExpiredK);
        }});

        List<Map<String, Object>> expiredLinks = cmsInternalRepositoryService().getExpiredLinks(query, 0, 10);

        assertNotNull(expiredLinks);
        assertEquals(download1, expiredLinks.get(0));

        Map<String, Object> dbObject2 = cmsInternalRepositoryService().findById("222222222");

        assertNotNull(dbObject2);
        assertEquals(download2, dbObject2);
    }
}
