# CMS - TAP downloadable files repository service.

## Required Software

    1) Java 1.7+
    2) MongoDB 1.8.1 or Latest
    3) Maven 3.0.3
    4) Tomcat server

## CMS - OS, proxy, tomcat related configurations

    1) Add the following DNS's to the /etc/hosts

        127.0.0.1   core.cms
        127.0.0.1   db.mongo.cms
        127.0.0.1   db.mongo.sdp
        127.0.0.1   core.sdp

    2) Add proxy pass and reverse proxy in apache2 as mention below

        ProxyPass /cms http://127.0.0.1:4287/cms
        ProxyPassReverse /cms http://127.0.0.1:4287/cms

    3) Tomcat server should be running in 4287 port
        <tomcat-install-dir>/conf/server.xml

         <Connector port="4287" protocol="HTTP/1.1"
                       connectionTimeout="20000"
                       redirectPort="2443" />

## CMS Build and Deploy

    ### Available profiles

      dialog-live
      dialog
      vodafone
      vcity-live
      vcity
      etisalat
      robi-bd
      robi-bd-live

     1) Go inside the modules directory, and give the following commands

        $ mvn clean install -DskipTests -P<profile-id>

        profile-id should be replace by one of the available profile id

     2) Stop running apache-tomcat server

     3) Copy the modules/cms-web/target/cms.war to apache-tomcat/webapps

     4) Then clean the apache-tomcat/work and start the server

     5) Now the CMS has been started. You may find the logs in the following directory

        $ cd /hms/logs/cms/
