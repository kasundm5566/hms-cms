use cms;

var collectionNames = db.getCollectionNames();

for(var i = 0; i < collectionNames.length; i++) {
	if(collectionNames[i].indexOf("temp") == 0) {
		printjson("Deleting temp collection " + collectionNames[i]);
		db[collectionNames[i]].drop();	
	}
}
