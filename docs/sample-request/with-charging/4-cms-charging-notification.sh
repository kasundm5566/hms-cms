#!/bin/sh

curl -v -H 'Content-Type:application/json' -X POST -d \
'{"statusCode":"S1000",
"timeStamp":"10-Oct-2011 14:30",
"externalTrxId":"1312071712270040",
"statusDetail":"Partial payment successful. Due amount fully paid.",
"applicationId":"APP_1000002",
"totalAmount":"11.00",
"paidAmount":"11.00",
"referenceId":"121339",
"balanceDue":"0.00",
"currency":"KES",
"internalTrxId":"112051416130016",
"version":"1.0"}' http://core.cms:4287/cms/cms-service/download/charging-notification

echo

### Response : {"statusCode":"S1000","statusDescription":"Success"}
