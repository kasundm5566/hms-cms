#!/bin/sh

curl -v -H 'Content-Type:application/json' -X POST -d \
'{"downloadRequestId":"1312071712270040",
"paymentInstrumentName":"Equity Bank"}' http://core.cms:4287/cms/cms-service/download/charge

echo

### Response - {"statusCode":"P1502","shortDescription":"Please access Equity Bank Easy Pay; enter following: Business no:111111, Reference ID:121339, Amount:Ksh 11","longDescription":"Please access Equity Bank Easy Pay menu and enter following when prompted: Business no:111111, Reference ID:121339, Amount:Ksh11. Please pay within 2 days."}
