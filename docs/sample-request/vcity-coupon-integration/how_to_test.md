# Execute query content script

    ## Edit the script cms-query-content.sh, and set an existing approved downloadable application id

    ## Run script

    sh cms-query-content.sh

    Example output:

    {"statusCode":"S1000","result":{"APP_000011":{"devices":{"HTC":[{"model":"My Touch 3G","content":[{"content-id":"1315072119360001","version":""}]},{"model":"My Touch 4G","content":[{"content-id":"1315072119360001","version":""}]}],"Samsung":[{"model":"Galaxy Nexus","content":[{"content-id":"1315072119360001","version":""}]},{"model":"Galaxy S","content":[{"content-id":"1315072119360001","version":""}]},{"model":"Galaxy S II","content":[{"content-id":"1315072119360001","version":""}]},{"model":"Galaxy Y","content":[{"content-id":"1315072119360001","version":""}]}]},"platforms":{"Android":[{"version":"2.1","content":[{"content-id":"1315072119360001","version":""}]},{"version":"3.0","content":[{"content-id":"1315072119360001","version":""}]}]}}},"statusDescription":"Success"}

    ## Select one of the content and note the content id

    "content-id":"1315072119360001"


# Execute download script

    ## Edit the script cms-download.sh, and put app id and content id got from previous request.

    ## Run script

    sh cms-download.sh

    Example output:

    {"statusCode":"P1501","statusDescription":"Select a payment instrument to do the charging","paymentInstrumentList":["Coupon","Mobile Account"],"downloadRequestId":"1315100915330013"}

    "downloadRequestId":"1315100915330013"

# Execute the download charge request

    ## Edit the script cms-download-charge.sh, and set downloadRequestId got from previous request.

    ## Run script

    sh cms-download-charge.sh

    Example output:
