db.things.save({"supported-devices" : [    {       "models" : [    "All Android 2.1",      "All Android 2.2" ],    "brand" : "all" } ], "version" : "", "description" : "", "name" : "Build1", "platform-versions" : [ "2.1", "2.2" ], "app-file-id" : "1312050916150001", "platform-name" : "Android" })

db.things.save({"supported-devices" : [    {       "models" : [    "Galaxy Y",     "Galaxy Nexus",         "Galaxy S" ],   "brand" : "Samsung" } ], "version" : "", "description" : "", "name" : "Build2", "platform-versions" : [ "2.2", "3.1" ], "app-file-id" : "1312050916150002", "platform-name" : "Android" })

db.things.save({"supported-devices" : [ { "models" : [ "Galaxy Nexus",     "Galaxy Nexus 1" ], "brand" : "Samsung" } ], "version" : "", "description" : "", "name" : "Build3", "platform-versions" : [ "3.1" ], "app-file-id" : "1312050916280003", "platform-name" : "Android" })

db.things.save({"supported-devices" : [    {       "models" : [    "Galaxy Y",     "Galaxy Nexus",         "Galaxy S II",  "Galaxy S" ],   "brand" : "Samsung" },  {       "models" : [    "My Touch 3G" ],        "brand" : "HTC" } ], "version" : "", "description" : "", "name" : "Build4", "platform-versions" : [ "2.1", "4.0" ], "app-file-id" : "1312050916320004", "platform-name" : "Android" })

m = function(){
    var id=this["app-file-id"];
    var version=this["version"];
    var platform=this["platform-name"];
    var platformVersions=this["platform-versions"];

    platformVersions.forEach(
        function(platformVersion){
            emit({platform:platform, platformVersion:platformVersion}, { content : [{contentId:id , version:version}]  });
        }
    );
};

r = function( key , values ){

    var content=[];

    var maxIdIndex=0;
    var maxValuesIndex=0;
    var max=0;

    for ( var i=0; i<values.length; i++ )  {
        for ( var j=0; j<values[i].content.length; j++ )  {
            var current = parseFloat(values[i].content[j].contentId);
            if (max < current) {
                maxValuesIndex= i;
                maxIdIndex= j;
                max = current;
            }
        }
    }

    content.push({contentId:values[maxValuesIndex].content[maxIdIndex].contentId,version:values[maxValuesIndex].content[maxIdIndex].version});

    return { content : content  };
};

res = db.things.mapReduce(m, r, { out : "temp" } );

db.temp.find()

m = function(){
    var platform=this._id.platform;
    var platformVersion=this._id.platformVersion;
    var content=this.value.content;

    emit(platform, {versions: [{version:platformVersion, content:content}]});
};

r = function( key , values ){

    var allVersions=[];

    for ( var i=0; i<values.length; i++ )
        for ( var j=0; j<values[i].versions.length; j++ )
            allVersions.push({version:values[i].versions[j].version, content:values[i].versions[j].content});

    return {versions:allVersions};
};

f = function(key, value){

    var versions={};

    for ( var i=0; i<value.versions.length; i++ ){
        versions[value.versions[i].version]=value.versions[i].content;
    }

    return versions;
};

res = db.temp.mapReduce(m, r, { out : "temp" ,finalize:f } );

db.temp.find()

db.dropDatabase()

