db.things.save({"supported-devices" : [    {       "models" : [    "All Android 2.1",      "All Android 2.2" ],    "brand" : "all" } ], "version" : "", "description" : "", "name" : "Build1", "platform-versions" : [ "2.1", "2.2" ], "app-file-id" : "1312050916150001", "platform-name" : "Android" })

db.things.save({"supported-devices" : [    {       "models" : [    "Galaxy Y",     "Galaxy Nexus",         "Galaxy S" ],   "brand" : "Samsung" } ], "version" : "", "description" : "", "name" : "Build2", "platform-versions" : [ "2.2", "3.1" ], "app-file-id" : "1312050916150002", "platform-name" : "Android" })

db.things.save({"supported-devices" : [ { "models" : [ "Galaxy Nexus",     "Galaxy Nexus 1" ], "brand" : "Samsung" } ], "version" : "", "description" : "", "name" : "Build3", "platform-versions" : [ "3.1" ], "app-file-id" : "1312050916280003", "platform-name" : "Android" })

db.things.save({"supported-devices" : [    {       "models" : [    "Galaxy Y",     "Galaxy Nexus",         "Galaxy S II",  "Galaxy S" ],   "brand" : "Samsung" },  {       "models" : [    "My Touch 3G" ],        "brand" : "HTC" } ], "version" : "", "description" : "", "name" : "Build4", "platform-versions" : [ "2.1", "4.0" ], "app-file-id" : "1312050916320004", "platform-name" : "Android" })

m = function(){
    var id=this["app-file-id"];
    var version=this["version"];
    var devices=this["supported-devices"];

    devices.forEach(
        function(device){
            var brand = device.brand;

            device.models.forEach(
                function(model){
                    emit({brand:brand, model:model}, { content : [{contentId:id , version:version}]  });
                }
            );
        }
    );
};

r = function( key , values ){

    var content=[];

    var maxIdIndex=0;
    var maxValuesIndex=0;
    var max=0;

    for ( var i=0; i<values.length; i++ )  {
        for ( var j=0; j<values[i].content.length; j++ )  {
            var current = parseFloat(values[i].content[j].contentId);
            if (max < current) {
                maxValuesIndex= i;
                maxIdIndex= j;
                max = current;
            }
        }
    }

    content.push({contentId:values[maxValuesIndex].content[maxIdIndex].contentId,version:values[maxValuesIndex].content[maxIdIndex].version});

    return { content : content  };
};

res = db.things.mapReduce(m, r, { out : "temp" } );

db.temp.find()

m = function(){
    var brand=this._id.brand;
    var model=this._id.model;
    var content=this.value.content;

    emit(brand, {models: [{model:model, content:content}]});
};

r = function( key , values ){

    var allModels=[];

    for ( var i=0; i<values.length; i++ )
        for ( var j=0; j<values[i].models.length; j++ )
            allModels.push({model:values[i].models[j].model, content:values[i].models[j].content});

    return {models:allModels};
};

f = function(key, value){

    var models={};

    for ( var i=0; i<value.models.length; i++ ){
        models[value.models[i].model]=value.models[i].content;
    }

    return models;
};

res = db.temp.mapReduce(m, r, { out : "temp" ,finalize:f } );

db.temp.find()

db.dropDatabase()
