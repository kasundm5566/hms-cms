db.things.save({ "_id" : "101111232020460001", "app-id" : "APP_912202", "content-id" : "101111232020460001", "description" : "Download Application", "vendor" : "Huawei", "model" : "U8230", "wapUrl" : "http://m.infindo.com/polygon388/d/0A07Pb", "message" : "Virtual City", "sender-address" : "hewani" })

db.things.save({ "_id" : "101111232020460002", "app-id" : "APP_912202", "content-id" : "101111232020460002", "description" : "Download Application", "vendor" : "Motorola", "model" : " XT810", "wapUrl" : "http://m.infindo.com/polygon388/d/1B4gp0", "message" : "Virtual City", "sender-address" : "hewani" })

db.things.save({ "_id" : "101111232020460003", "app-id" : "APP_912202", "content-id" : "101111232020460003", "description" : "Download Application", "vendor" : "Motorola", "model" : " DEFY", "wapUrl" : "http://m.infindo.com/polygon388/d/B3m71i", "message" : "Virtual City", "sender-address" : "hewani" })

db.things.save({ "_id" : "101111232020460004", "app-id" : "APP_912202", "content-id" : "101111232020460004", "description" : "Download Application", "vendor" : "Motorola", "model" : " BACKFLIP", "wapUrl" : "http://m.infindo.com/polygon388/d/9fW4vY", "message" : "Virtual City", "sender-address" : "hewani" })


m = function(){
    emit({brand:this.vendor, model:this.model}, { content : [{contentId:this["content-id"] , version:""}] });
};

r = function( key , values ){

    var content=[];

    var maxIdIndex=0;
    var maxValuesIndex=0;
    var max=0;

    for ( var i=0; i<values.length; i++ )  {
        for ( var j=0; j<values[i].content.length; j++ )  {
            var current = parseFloat(values[i].content[j].contentId);
            if (max < current) {
                maxValuesIndex= i;
                maxIdIndex= j;
                max = current;
            }
        }
    }

    content.push({contentId:values[maxValuesIndex].content[maxIdIndex].contentId,version:values[maxValuesIndex].content[maxIdIndex].version});

    return { content : content  };
};

res = db.things.mapReduce(m, r, { out : "temp" } );

db.temp.find()

m = function(){
    var brand=this._id.brand;
    var model=this._id.model;
    var content=this.value.content;

    emit(brand, {models: [{model:model, content:content}]});
};

r = function( key , values ){

    var allModels=[];

    for ( var i=0; i<values.length; i++ )
        for ( var j=0; j<values[i].models.length; j++ )
            allModels.push({model:values[i].models[j].model, content:values[i].models[j].content});

    return {models:allModels};
};

f = function(key, value){

    var models={};

    for ( var i=0; i<value.models.length; i++ ){
        models[value.models[i].model]=value.models[i].content;
    }

    return models;
};

res = db.temp.mapReduce(m, r, { out : "temp" ,finalize:f } );

db.temp.find()

db.dropDatabase()

