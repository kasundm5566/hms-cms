# Check list for provisioning new profile creations

## Prerequisites

$profile_id - should be replaced by actual profile id - Ex: dialog, vcity
$operator_id - should be replaced by actual operator id - Ex : dialog, safaricom
$alias - alias for cms app mt
$default_sender_address - default sender address
$service_charge_percentage - service charge percentage for cas
## Maven profile

1. Add new profile to modules/cms-web project

<profile>
   <id>$profile_id-live</id>
   <properties>
       <resource.file.location>profile/$profile_id/live</resource.file.location>
   </properties>
</profile>
<profile>
   <id>$profile_id</id>
   <properties>
       <resource.file.location>profile/$profile_id/da</resource.file.location>
   </properties>
</profile>

2. Create configuration directories inside - modules/cms-web/src/main/profile

├── $profile_id
│   ├── da
│   │   └── resources
│   │       ├── cms-beans.xml
│   │       ├── cms-operator-beans.xml
│   │       └── cms.properties
│   └── live
│       └── resources
│           ├── cms-beans.xml
│           ├── cms-operator-beans.xml
│           └── cms.properties

## Configurations

3. cms-beans.xml

3.1 Add additional parameters which need to be included in the NBL cas request.
If no additional parameters don't include this bean.

<util:map id="additionalParams" value-type="java.lang.String">
</util:map>

4. cms-operator-beans.xml

4.1 Create the download channel - handler mapping

<util:map id="cms.service.downloadUrlHandlerMap">
</util:map>

Available channels

web
wap
android
ussd

Available handlers

sms handler
wap-push handler
direct download handler

5. cms.properties

5.1 Set default download channel - to support legacy api before channel based download.

cms.default.download.channel
Ex:
cms.default.download.channel=web

5.2 Localization and branding names

cms.internal.app.wap.push.message
cms.internal.app.sms.send.message
cms.system.appstore.name
cms.mct.exceed.error
cms.tpd.exceed.error
cms.content.not.found.error
cms.url.expired.error
cms.download.retry.count.exceed.error

5.3 Currency

cms.system.currency=LKR

6. Create a initial data directory for the profile.

├── $profile_id
│   ├── cms-app.json
│   ├── cms_init_data.sh
│   └── cms-ncs-sla.json

6.1 cms-app.json

"ncses" : [
            {
              "status": "ncs-configured",
              "ncs-type": "cas"
            },
            {
              "status": "ncs-configured",
              "ncs-type": "wap-push",
              "operator": "$operator_id"
            },
            {
              "status": "ncs-configured",
              "ncs-type": "sms",
              "operator": "$operator_id"
            }
]

6.2 cms-ncs-sla.json

sms sla
{
  "mt": {
    "aliasing": [
      $alias
    ],
    "default-sender-address": $default-sender-address,
  },

  "operator": "$operator_id"
}

cas sla
{
  "charging": {
    "allowed-payment-instruments": [
      {
        "service-charge-percentage": "30",
        "name": "Mobile Account"
      }
    ]
  }
}

{
  "mt": {
    "aliasing": [
      $alias
    ],
    "default-sender-address": $default-sender-address,
    "tps": "10"
  },
  "operator": $operator_id
}

wap-push sla

{
  "mt": {
    "aliasing": [
      $alias
    ],
    "default-sender-address": $default-sender-address,
  },
  "operator": $operator_id,
}